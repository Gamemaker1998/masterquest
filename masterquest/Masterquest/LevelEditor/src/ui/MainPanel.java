/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import background.Level;
import background.Texture;
import background.Unit;
import background.Vector2f;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.WindowConstants;

/**
 *
 * @author Dynamitos5
 */
public class MainPanel extends Canvas {
    public static final Vector2f GRAVITY = new Vector2f(0f, 9.81f);
    private JFrame jframe;
    private int width, height;
    private BufferStrategy bs;
    private Level level;
    private BufferedImage frame;
    private int[] pixels;
    private Graphics graphics;
    private final int offX = 1, offY = 30;
    private int diameter = 100;
    public static volatile boolean running;

    public static void main(String[] args) {
        new MainPanel().start();
    }

    public void start() {
        init();
        loop();
        clear();
    }

    public void init() {
        jframe = new JFrame("Level Editor");
        width = 800;
        height = 600;
        jframe.setSize(width, height);
        jframe.setLayout(new GridLayout(0, 1));
        jframe.add(this);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(3);
        jframe.createBufferStrategy(3);
        jframe.addMouseListener(new MouseListener() {
            @Override
            public void mousePressed(MouseEvent e) {
                level.addUnit(new Unit(new Vector2f(e.getX()-offX, e.getY()-offY), Texture.loadTexture("src/background/grass.png"), 100, 100, false));
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        bs = jframe.getBufferStrategy();
        if (bs == null) {
            System.exit(-1);
        }
        level = new Level();
        frame = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        pixels = ((DataBufferInt)frame.getRaster().getDataBuffer()).getData();
    }

    public void loop() {
        running = true;
        List<Unit> units = level.getUnits();
        while(running)
        {
            if(bs == null)
            {
                createBufferStrategy(3);
                break;
            }
            for(Unit unit : units){
                
            }
            graphics = bs.getDrawGraphics();
            graphics.drawImage(frame, 0, 0, width, height, null);
            graphics.dispose();
            bs.show();
        }
        /*while (running) {
            do {
                do {
                    Graphics graphics = bs.getDrawGraphics();
                    graphics.clearRect(0, 0, width, height);
                    for (int i = 0; i < units.size(); i++) {
                        Unit unit = units.get(i);
                        unit.update();
                        Texture tex = unit.getTexture();
                        try {
                            BufferedImage image = new BufferedImage(tex.getWidth(), tex.getHeight(), BufferedImage.TYPE_INT_ARGB);
                            image.setRGB(0, 0, image.getWidth(), image.getHeight(), tex.getPixels(), 0, image.getWidth());
                            graphics.drawImage(image, offX + (int) unit.getPosition().getX(), offY + (int) unit.getPosition().getY(), unit.getWidth(), unit.getHeight(), this);
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    graphics.drawImage(frame, 0, 0, this);
                    graphics.dispose();

                } while (bs.contentsRestored());

                bs.show();

            } while (bs.contentsLost());
        }*/
    }

    public void clear() {

    }
}
