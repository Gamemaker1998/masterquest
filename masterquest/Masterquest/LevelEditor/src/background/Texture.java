package background;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class Texture {
    private int[] pixels;
    private int width, height;
    public Texture(int width, int height){
        pixels = new int[width * height];
        this.width = width;
        this.height = height;
    }
    public static Texture loadTexture(String textureName){
        Texture tex = null;
        try{
            BufferedImage image = ImageIO.read(new File(textureName));
            tex = new Texture(image.getWidth(), image.getHeight());
            image.getRGB(0, 0, image.getWidth(), image.getHeight(), tex.getPixels(), 0, image.getWidth());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return tex;
    }

    public int[] getPixels() {
        return pixels;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setPixels(int[] pixels) {
        this.pixels = pixels;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
}
