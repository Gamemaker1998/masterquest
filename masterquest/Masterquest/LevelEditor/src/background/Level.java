/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package background;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dynamitos5
 */
public class Level {
    private List<Unit> units = new ArrayList<>();
    
    /*
    *    @param unit die Unit die an der Stelle position eingefügt werden soll
    */
    public void addUnit(Unit unit){
        units.add(unit);
    }
    
    /**
     *
     * @param unit die zu löschende Unit
     */
    public void removeUnit(Unit unit){
        units.remove(unit);
    }

    /**
     *
     * @param position die position des zu löschenden Units
     */
    public void removeUnit(Vector2f position){
        
    }

    public List<Unit> getUnits() {
        return units;
    }
}
