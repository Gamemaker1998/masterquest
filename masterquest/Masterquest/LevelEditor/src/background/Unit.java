/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package background;

import ui.MainPanel;

/**
 *
 * @author Dynamitos5
 */
public class Unit {

    private Vector2f position;
    private Texture texture;
    private int width, height;
    private Vector2f speed;
    private boolean hasPhysics;

    public Unit(Vector2f position, Texture texture, int width, int height, boolean physics) {
        this.texture = texture;
        this.position = position;
        this.width = width;
        this.height = height;
        this.hasPhysics = physics;
        speed = new Vector2f();
    }

    public void update() {
        if (hasPhysics) {
            speed.add(MainPanel.GRAVITY);
        }
        position.add(speed);
        if (position.getY() <= height) {
            position.setY(height);
        }
    }
    public void drawToArray(int[] pixels){
        
    }
    public void move(Vector2f movement) {
        speed.add(movement);
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Vector2f getPosition() {
        return position;
    }

    public Texture getTexture() {
        return texture;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
