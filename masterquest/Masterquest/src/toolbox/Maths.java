package toolbox;

import engine.data.util.vector.Matrix4f;
import engine.data.util.vector.Vector3f;

public class Maths
{
	public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f rotation, Vector3f dimension)
	{
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.rotate((float)(Math.toRadians(rotation.x)), new Vector3f(1, 0, 0), matrix, matrix);
		Matrix4f.rotate((float)(Math.toRadians(rotation.y)), new Vector3f(0, 1, 0), matrix, matrix);
		Matrix4f.rotate((float)(Math.toRadians(rotation.z)), new Vector3f(0, 0, 1), matrix, matrix);
		Matrix4f.scale(new Vector3f(dimension.x, dimension.y, dimension.z), matrix, matrix);
		return matrix;
	}
	
	public static Matrix4f createViewMatrix(Vector3f translation)
	{
		Matrix4f matrix = new Matrix4f();
		// Sets this matrix to the current matrix
		matrix.setIdentity();
		Matrix4f.translate(translation, matrix, matrix);
		return matrix;
	}
}
