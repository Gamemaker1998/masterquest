package manager;

import engine.Engine;


/**
 * Virtual Data Representor returns graphics model in a better way
 * @author yan
 *
 */
public final class VDR extends Manager
{
	@Override
	public void update()
	{

	}

	@Override
	public void update(int index)
	{

	}

	@Override
	public void update(int[] indices)
	{

	}
}
