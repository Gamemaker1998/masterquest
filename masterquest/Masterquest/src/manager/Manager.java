package manager;

public abstract class Manager
{
	public abstract void update();
	public abstract void update(int index);
	public abstract void update(int[] indices);
}
