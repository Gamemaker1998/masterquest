package engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import engine.data.Transform;
import engine.data.Vector3D;
import engine.renderEngine.TextureLegacy;
import engine.renderEngine.shaders.ShaderModel;
import entity.Entity;
import exception.ToFewArgumentsException;


public class FIOReader
{
	// Block data file configurations
	private static final String BLOCK_DATA_FILE_NAME = "test2.bdata";
	private static final String DELIMITER = "/";
	private static final int AMOUNT_OF_TOKENS = 7;

	public ArrayList<Entity> read()
	{
		return read(BLOCK_DATA_FILE_NAME);
	}

	@SuppressWarnings("finally")
	public ArrayList<Entity> read(String string)
	{
		Entity currentEntity;
		File file = new File(string);
		Vector3D position;
		Vector3D dimension;
		Vector3D rotation;
		TextureLegacy texture;
		ArrayList<Entity> entities = new ArrayList<>();
		BufferedReader bufferedReader = null;

		try
		{
			bufferedReader = new BufferedReader(new FileReader(file));
			String line;
			while((line = bufferedReader.readLine()) != null)
			{
				String[] tokens = line.split(DELIMITER);
				if(tokens.length < AMOUNT_OF_TOKENS)
					throw new ToFewArgumentsException("To few arguments in file: " + file.getName());
				position = new Vector3D(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]) + 100, 0.0f);
				dimension = new Vector3D(Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]), 0.0f);
				rotation = new Vector3D(0.0f, 0.0f, 0.0f);
				texture = TextureLegacy.loadBitmap("Grass_Mid.png");
				if(entities.size() == 0)
					//test2: Entity.mainCameraPosition.setVector3D(position.getXCoord()+10, position.getYCoord()+1, position.getZCoord());
					//testFlo: Entity.mainCameraPosition.setVector3D(position.getXCoord(), position.getYCoord()+10, position.getZCoord());
					//Entity.mainCamera.getTransform().getPosition().setVector3f(position.getXCoord() + 10, position.getYCoord() + 10, position.getZCoord());
				//currentEntity = new Entity(new Transform(position, dimension, rotation)).addShaderModel(new ShaderModel(texture)).addPhysicsComponent();
				currentEntity.getPhysicsComponent().setKinetic(Boolean.parseBoolean(tokens[4])).setGravity(Boolean.parseBoolean(tokens[5]));
				currentEntity.getPhysicsComponent().s = "aaaa";
				Engine.setDataOperationFlag(true);
				Engine.physicManager.addPhysicsComponent(currentEntity.getPhysicsComponent());

				entities.add(currentEntity);
			}
			Engine.setDataOperationFlag(false);
			bufferedReader.close();
			return entities;
		}
		catch (FileNotFoundException e)
		{
			System.err.println("File " + file.getName() + "was not found");
			Engine.setDataOperationFlag(false);
			return null;
		}
		catch (IOException e)
		{
			System.err.println("IOException at file " + file.getName());
			Engine.setDataOperationFlag(false);
			return null;
		}
		catch (ToFewArgumentsException e)
		{
			System.err.println(e.getMessage());
			Engine.setDataOperationFlag(false);
			return null;
		}
		finally
		{
			try
			{
				bufferedReader.close();
			}
			catch (IOException e)
			{
				System.err.println("Error at closing bufferedReader " + e.getMessage());
			}
		}
	}
}
