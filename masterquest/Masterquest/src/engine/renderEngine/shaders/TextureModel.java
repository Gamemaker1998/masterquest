package engine.renderEngine.shaders;

public class TextureModel
{
	private int textureID;
	private boolean isLoaded;
	
	public TextureModel(int id)
	{
		this.textureID = id;
	}
	
	public TextureModel setLoaded(boolean isLoaded)
	{
		this.isLoaded = isLoaded;
		return this;
	}
	
	public int getID()
	{
		return textureID;
	}
	
	public boolean isLoaded()
	{
		return isLoaded;
	}
}
