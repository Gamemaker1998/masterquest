package engine.renderEngine.shaders;

import engine.data.util.vector.Matrix4f;

public class StaticShader extends ShaderProgram
{
	private static final String VERTEX_SHADER_FILE = "src\\engine\\renderEngine\\shaders\\vertexShader.txt";
	private static final String FRAGMENT_SHADER_FILE = "src\\engine\\renderEngine\\shaders\\fragmentShader.txt";
	
	private int locationTransformationMatrix;
	private int locationViewMatrix;
	private int locationProjectionMatrix;
	
	public StaticShader()
	{
		super(VERTEX_SHADER_FILE, FRAGMENT_SHADER_FILE);
	}

	@Override
	protected void bindAttributes()
	{
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}

	@Override
	protected void getAllUniformLocations()
	{
		locationTransformationMatrix = super.getUniformLocation("transformationMatrix");
		locationViewMatrix = super.getUniformLocation("viewMatrix");
		locationProjectionMatrix = super.getUniformLocation("projectionMatrix");
	}
	
	public void loadTransformationMatrix(Matrix4f matrix)
	{
		// To move an object in OpenGL the matrix is added to the object's location
		super.loadMatrix(locationTransformationMatrix, matrix);
	}
	
	public void loadViewMatrix(Matrix4f matrix)
	{
		// To move the camera to a different location
		super.loadMatrix(locationViewMatrix, matrix);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix)
	{
		// To have the entity appear smaller when the distance to the camera is bigger
		super.loadMatrix(locationProjectionMatrix, matrix);
	}
}
