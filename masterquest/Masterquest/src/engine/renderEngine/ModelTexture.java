package engine.renderEngine;

public class ModelTexture
{
	// Represents the default texture ID
	private int textureID;
	// Can handle 15 other textures
	private int[] textureIDs = new int[15];
	// Check if the model texture has already being loaded in order to prevent fails
	private boolean isLoaded;
	
	public ModelTexture(int textureID)
	{
		this.textureID = textureID;
	}
	
	public ModelTexture setTexture(int textureID)
	{
		this.textureID = textureID;
		return this;
	}
	
	public ModelTexture setLoaded(boolean isLoaded)
	{
		this.isLoaded = isLoaded;
		return this;
	}
	
	public int getTextureID()
	{
		return textureID;
	}
	
	public boolean isLoaded()
	{
		return isLoaded;
	}
}
