package engine.renderEngine;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import engine.renderEngine.shaders.Texture;


public class VAOLoader
{
	private static class VAOBufferObject
	{
		private RawModel rawModel;
		private float[] positions;
		private float[] textureCoords;
		private int[] indices;

		public VAOBufferObject(RawModel rawModel, float[] positions, float[] textureCoords, int[] indices)
		{
			this.rawModel = rawModel;
			this.positions = positions;
			this.textureCoords = textureCoords;
			this.indices = indices;
		}
	}
	
	private static class TextureBufferObject
	{
		private ModelTexture modelTexture;
		private String filename;
		
		public TextureBufferObject(ModelTexture modelTexture, String filename)
		{
			this.modelTexture = modelTexture;
			this.filename = filename;
		}
	}

	private Set<Integer> vaoIDs;
	private Set<Integer> vboIDs;
	private Set<Integer> textureIDs;

	private static Lock lock;

	private List<VAOBufferObject> VAOBufferObjects;
	private List<TextureBufferObject> textureBufferObjects;
	
	private Map<String, ModelTexture> modelTextures;

	public VAOLoader()
	{
		vaoIDs = new HashSet<>();
		vboIDs = new HashSet<>();
		textureIDs = new HashSet<>();

		lock = new ReentrantLock();

		VAOBufferObjects = new ArrayList<>();
		textureBufferObjects = new ArrayList<>();

		RawModel.loadConstantRawModels(this);
	}

	public RawModel createVAOBuffer(float[] positions, float[] textureCoords, int[] indices)
	{
		lock.lock();

		VAOBufferObject vaoBO = new VAOBufferObject(new RawModel(-1, -1), positions, textureCoords, indices);
		VAOBufferObjects.add(vaoBO);

		lock.unlock();
		
		return vaoBO.rawModel;
	}

	public ModelTexture createTextureModel(String filename)
	{
		lock.lock();

		TextureBufferObject tbo = new TextureBufferObject(new ModelTexture(-1), filename);
		textureBufferObjects.add(tbo);

		lock.unlock();
		
		return tbo.modelTexture;
	}

	public void updateBuffers()
	{
		lock.lock();

		for(VAOBufferObject vaoBufferObject: VAOBufferObjects)
		{
			loadToVAO(vaoBufferObject.rawModel, vaoBufferObject.positions, vaoBufferObject.textureCoords, vaoBufferObject.indices);
		}
		VAOBufferObjects.clear();

		for(TextureBufferObject tbo: textureBufferObjects)
		{
			loadToTexture(tbo.modelTexture, tbo.filename);
		}
		textureBufferObjects.clear();

		lock.unlock();
	}

	private RawModel loadToVAO(RawModel rawModel, float[] positions, float[] textureCoords, int[] indices)
	{
		// We want to create a VAO and store the vaoID for the RawModel
		int vaoID = createVAO();
		bindIndicesBuffer(indices);
		// The positions are stored into the attribute list on index 0
		storeDataInAttributeList(0, 3, positions);
		storeDataInAttributeList(1, 2, textureCoords);
		// Here the VAO gets unbound meaning that it is not going to be configured
		unbindVAO();

		return (rawModel.set(vaoID, indices.length).setLoaded(true));
	}

	private ModelTexture loadToTexture(ModelTexture modelTexture, String fileName)
	{
		Texture texture = new Texture(fileName + ".png");
		textureIDs.add(texture.getTextureID());

		return (modelTexture.setTexture(texture.getTextureID()).setLoaded(true));
	}

	private int createVAO()
	{
		// Here a vertex array is created and the vaoID is returned to the field
		int vaoID = GL30.glGenVertexArrays();
		vaoIDs.add(vaoID);
		/* If a VAO is bound it means that it is set as the current VAO meaning that 
		 * whenever something is configured it happens to be the current VAO
		 */
		GL30.glBindVertexArray(vaoID);

		return vaoID;
	}

	/**
	 * The data here gets stored in a VBO which is an object of the 15 objects
	 * of the VAO.
	 * 
	 * @param index
	 * @param positions
	 */
	private void storeDataInAttributeList(int attributeNumber, int coordinateSize, float[] data)
	{
		// Creates a VBO and returns the VBOID to store it in the field
		int vboID = GL15.glGenBuffers();
		vboIDs.add(vboID);
		// Here the VBO is bound meaning that the VBO with the given ID will be currently used
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		// Here the data is converted into a FloatBuffer
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		/* There the currently used VBO (the bound one) gets the buffer stored in it
		 * The GL_STATIC_DRAW means that the data will not be changed so OpenGL knows that it can just render it
		 */
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
		// Unbind the VBO buffer
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}

	private FloatBuffer storeDataInFloatBuffer(float[] data)
	{
		// A FloatBuffer is made and the total length it should have is given to it
		FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
		// I put the data into the buffer
		floatBuffer.put(data);
		// The flip signalises that the buffer isn't longer in write mode but in read mode
		floatBuffer.flip();

		return floatBuffer;
	}

	private void bindIndicesBuffer(int[] indices)
	{
		int vboID = GL15.glGenBuffers();
		vboIDs.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}

	private void unbindVAO()
	{
		// To unbind the vertex array bind it with 0 meaning that the current used VAO isn't set to current anymore
		GL30.glBindVertexArray(0);
	}

	private IntBuffer storeDataInIntBuffer(int[] data)
	{
		// Create an empty IntBuffer with the same size as the data array
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		// Flip to make it ready to be read
		buffer.flip();

		return buffer;
	}
	
	
	public ModelTexture getModelTexture(String filename)
	{
		return modelTextures.get(filename);
	}

	/**
	 * To delete all VAOs and VBOs
	 */
	public void cleanUp()
	{
		for(int vaoID: vaoIDs)
		{
			GL30.glDeleteVertexArrays(vaoID);
		}
		for(int vboID: vboIDs)
		{
			GL15.glDeleteBuffers(vboID);
		}
		for(int textureID: textureIDs)
		{
			GL11.glDeleteTextures(textureID);
		}
	}
}
