package engine.renderEngine;

public class RawModel
{
	private int vaoID;
	private int vertexCount;
	private boolean isLoaded;
	
	public static RawModel SQUARE_MODEL;
	
	public static void loadConstantRawModels(VAOLoader vaoLoader)
	{
		{//SQUARE_MODEL
			float vertices[] = 
				{
						-1f, 1f, 0.0f,
						-1f, -1f, 0.0f,
						1f, -1f, 0.0f,
						1f, 1f, 0.0f
				};
			
			float textureCoords[] =
				{
					0,0,	// V0
					0,1,	// V1
					1,1,	// V2
					1,0		// V3
				};
			
			int indices[] = 
				{
						0, 1, 3,
						3, 1, 2
				};
			SQUARE_MODEL = vaoLoader.createVAOBuffer(vertices, textureCoords, indices);
		}
		
		{//NEXT_MODEL....
			
		}
	}
	
	

	public RawModel(int vaoID, int vertexCount)
	{
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
	}

	public void setVaoID(int vaoID)
	{
		this.vaoID = vaoID;
	}

	public void setVertexCount(int vertexCount)
	{
		this.vertexCount = vertexCount;
	}
	
	public RawModel setLoaded(boolean isLoaded)
	{
		this.isLoaded = isLoaded;
		return this;
	}
	
	public RawModel set(int vaoID, int vertexCount)
	{
		setVaoID(vaoID);
		setVertexCount(vertexCount);
		return this;
	}

	public int getVaoID()
	{
		return vaoID;
	}

	public int getVertexCount()
	{
		return vertexCount;
	}
	
	public boolean isLoaded()
	{
		return isLoaded;
	}
}
