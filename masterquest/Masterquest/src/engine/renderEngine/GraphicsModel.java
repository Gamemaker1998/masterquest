package engine.renderEngine;

public class GraphicsModel
{
	private RawModel rawModel;
	private ModelTexture modelTexture;

	public GraphicsModel(RawModel rawModel, ModelTexture modelTexture)
	{
		this.rawModel = rawModel;
		this.modelTexture = modelTexture;
	}
	
	public GraphicsModel(){}

	public GraphicsModel setRawModel(RawModel rawModel)
	{
		this.rawModel = rawModel;
		return this;
	}

	public GraphicsModel setModelTexture(ModelTexture modelTexture)
	{
		this.modelTexture = modelTexture;
		return this;
	}

	public RawModel getRawModel()
	{
		return rawModel;
	}

	public ModelTexture getModelTexture()
	{
		return modelTexture;
	}
	
	public boolean isRenderable()
	{
		return (rawModel != null && modelTexture != null && rawModel.isLoaded() && modelTexture.isLoaded());
	}
}
