package engine.renderEngine;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryUtil;

import engine.MultiInitializable;
import engine.input.InputHandler;


public class Screen extends Thread implements MultiInitializable
{
	private String title;
	private int width, height;
	private boolean isInitialized;

	// We need to strongly reference callback instances.
	private GLFWErrorCallback errorCallback;
	public InputHandler inputHandler;

	// The window handle. Here the window ID is stored
	private long window;

	//TODO: Schlampig, renderManager sollte private erreichbar sein
	public RenderManager renderManager;

	public Screen(String title, int width, int height)
	{
		this.title = title;
		this.width = width;
		this.height = height;
		isInitialized = false;
	}

	/**
	 * Initialise OpenGL components
	 */
	public void init()
	{
		System.out.println("Initialising OpenGL...");

		// Setup an error callback. The default implementation like
		// we have here will print the error message in System.err
		GLFW.glfwSetErrorCallback(errorCallback = GLFWErrorCallback.createPrint(System.err));

		// Initialise GLFW. Without this nothing will be able to work
		if(GLFW.glfwInit() != GLFW.GLFW_TRUE)
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configuring the window
		GLFW.glfwDefaultWindowHints(); // This is optional due to the fact that the window hints are already set default
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE); // Hides the window
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE); // Enables the window to be resized

		// Creates the window where the width, the height, the title, a monitor and something else is given
		window = GLFW.glfwCreateWindow(width, height, title, MemoryUtil.NULL, MemoryUtil.NULL);
		if(window == MemoryUtil.NULL)
			throw new RuntimeException("Failed to create the GLFW window");

		// A key callback shall be setup. Every time a key is pressed, repeated or released it
		// will be called
		GLFW.glfwSetKeyCallback(window, (inputHandler = new InputHandler()));

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
		// With this code we can center our window
		GLFW.glfwSetWindowPos(window, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2);

		// Here we make the OpenGL context current based on the window ID
		GLFW.glfwMakeContextCurrent(window);
		/*
		 * This line is critical for LWJGL's interoperation with GLFW's
		 * OpenGL context, or any context that is managed externally.
		 * LWJGL detects the context that is current in the current thread,
		 * creates the GLCapabilities instance and makes the OpenGL
		 * bindings available for use.
		 */
		GL.createCapabilities();

		// We want to enable v-sync
		/*
		 * The value which is given as swap interval means how many screen updates should be waited until GLFW swaps the buffers
		 * Meaning: If the screen has 60Hz and the swap interval is 1 the frames will be 60 too (60/1).
		 * 			If the screen has 60Hz and the swap interval is 4 the frames will be 15 (60/4)
		 */
		GLFW.glfwSwapInterval(1);

		// To make the window visible
		GLFW.glfwShowWindow(window);

		// Set the clear color
		GL11.glClearColor(0, 0, 0, 1.0f);
		// To disable rendering things which are behind another pixel
		GL11.glEnable(GL11.GL_TRUE);

		System.out.println("Finished initialising OpenGL");

		// After finishing initialising OpenGL, the rest can be initialised
		renderManager = new RenderManager(width, height);
		
		// Finished initialising
		isInitialized = true;
	}

	@Override
	public void run()
	{
		init();

		while(GLFW.glfwWindowShouldClose(window) == GL11.GL_FALSE)
		{
			renderManager.render();

			// To swap the color buffer
			GLFW.glfwSwapBuffers(window);

			/* 
			 * Poll for window events. The key callback above will only be
			 * invoked during this call.
			 */
			GLFW.glfwPollEvents();
		}

		// To let the RenderManager clean up its resources and end its runtime
		renderManager.end();

		// To destroy the window and set the input handler free
		GLFW.glfwDestroyWindow(window);
		inputHandler.release();

		// To delete the first initialised GLFW context and set the error callback free
		GLFW.glfwTerminate();
		errorCallback.release();

		System.exit(1);
	}

	public static Screen createScreen(String title, int width, int height)
	{
		return new Screen(title, width, height);
	}

	public void setVisible(boolean visible)
	{
		if(isInitialized)
		{
			if(visible)
				GLFW.glfwShowWindow(window);
			else
				GLFW.glfwHideWindow(window);
		}
	}

	public void hideMouse(boolean hide)
	{
		if(isInitialized)
		{
			if(hide)
				GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_HIDDEN);
			else
				GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL);
		}
	}

	public void grabMouse(boolean grab)
	{
		if(isInitialized)
		{
			if(grab)
				GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED);
			else
				GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL);
		}
	}

	public void terminate()
	{
		if(isInitialized)
		{
			GLFW.glfwSetWindowShouldClose(window, GL11.GL_TRUE);
		}
	}
	
	
	
	@Override
	public boolean isInitialized()
	{
		return isInitialized;
	}
}
