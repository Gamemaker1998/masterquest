package engine;

@FunctionalInterface
public interface MultiInitializable
{
	boolean isInitialized();
}
