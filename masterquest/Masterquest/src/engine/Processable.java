package engine;

public interface Processable
{
	public void process();
}
