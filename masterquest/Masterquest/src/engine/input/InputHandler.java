package engine.input;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;


public class InputHandler extends GLFWKeyCallback
{
	private boolean[] keys;

	public InputHandler()
	{
		init();
	}

	public void init()
	{
		keys = new boolean[(int)(Math.pow(2, 30))];
	}

	@Override
	public void invoke(long window, int key, int scancode, int action, int mods)
	{
		if(action == GLFW.GLFW_PRESS)
			keys[key] = true;
		else if(action == GLFW.GLFW_RELEASE)
			keys[key] = false;
	}

	/**
	 * Returns true if the given key (The key code parameter matches the key
	 * code of GLFW.GLFW_KEY_....) is pressed and false if not
	 * 
	 * @param keyCode
	 * @return
	 */
	public boolean isPressed(int keyCode)
	{
		return this.keys[keyCode];
	}
}
