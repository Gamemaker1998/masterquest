package engine;

public class Logic
{
	public static boolean between(double point, double start, double range)
	{
		return (point > start && point < start + range);
	}
	
	public static boolean between(double point, double pRange, double start, double range)
	{
		return ((point >= start && point < start + range) || (point < start && point + pRange > start));
	}
	
	public static boolean in(double point, double start, double range)
	{
		return (point >= start && point <= start + range);
	}
}
