package engine;

import java.util.LinkedList;

import engine.physicEngine.PhysicManager;
import engine.renderEngine.Screen;
import manager.DataManager;
import soundEngine.SoundEngine;


public final class Engine extends Process implements Runnable // Das ist der Main-Process
{
	private static Thread mainThread;
	private static boolean run;

	public static Screen screen;
	public static DataManager dataManager;
	public static PhysicManager physicManager;
	public static SoundEngine soundEngine;

	private static boolean dataOperationFlag;

	private static int ticksPerMilliSecond;
	private static int ticksPerMilliSecond2;
	private static int currentTicksPerMilliSecond;
	private static long deltaTime;
	private static long deltaTime2;
	private static int currentTicks;
	private static int currentTicks2;
	private static long milliSleeper;

	private static final long FRAME_LIMITER = 1000 / 1000;

	public static synchronized void init()
	{
		deltaTime = 0;
		processes = new LinkedList<Process>();
		
		currentTicksPerMilliSecond = 100000;

		mainThread = new Thread(new Engine());
		milliSleeper = 0;

		screen = new Screen("Masterquest Game Engine", 1366, 768);
		dataManager = new DataManager();
		physicManager = new PhysicManager();
		soundEngine = new SoundEngine();

		screen.start();

		// Wait for the other initialisations
		// Check whether the MultiInitializable objects have all been initialised
		while(!screen.isInitialized())
		{
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				System.err.println("Error while waiting for initialisations");
			}
		}
	}

	public static void start()
	{
		mainThread.start();
		run = true;
	}

	public static void terminate()
	{
		if(run)
		{
			mainThread.interrupt();
		}
	}

	@Override
	public void run()
	{
		while(!mainThread.isInterrupted())
		{
			process();
		}
		run = false;
	}

	@Override
	public void process()
	{
		// TODO: Improving the calculation for the ticks per second
		try
		{
			Thread.sleep(milliSleeper);
		}
		catch (Exception e)
		{
		}
		if(deltaTime == 0)
		{
			deltaTime = System.currentTimeMillis();
			currentTicks = 0;
			return;
		}
		else if(deltaTime2 == 0)
		{
			if(System.currentTimeMillis() - deltaTime >= 500)
			{
				deltaTime2 = System.currentTimeMillis();
				currentTicks2 = 0;
			}
			return;
		}
		if(System.currentTimeMillis() - deltaTime >= 1000)
		{
			deltaTime = System.currentTimeMillis();
			ticksPerMilliSecond = currentTicks;
			currentTicksPerMilliSecond = ticksPerMilliSecond;
			System.out.println(ticksPerMilliSecond + " TPS");
			milliSleeper = FRAME_LIMITER;
			currentTicks = 0;
		}
		else if(System.currentTimeMillis() - deltaTime2 >= 1000)
		{
			deltaTime2 = System.currentTimeMillis();
			ticksPerMilliSecond2 = currentTicks2;
			currentTicksPerMilliSecond = ticksPerMilliSecond2;
			milliSleeper = FRAME_LIMITER;
			currentTicks2 = 0;
		}

		if(!dataOperationFlag)
		{
			for(Process process: processes)
			{
				process.process();
			}
			physicManager.physics();
		}
		soundEngine.playMusic();

		currentTicks++;
		currentTicks2++;
	}

	public static float getDeltaTime()
	{
		return (currentTicksPerMilliSecond == 0) ? (0) : (1001f / (currentTicksPerMilliSecond));
	}

	public static void setDataOperationFlag(boolean dataOperationFlag)
	{
		Engine.dataOperationFlag = dataOperationFlag;
	}

}
