package engine;


public interface Renderable
{
	public void render();
	public void render(int framesPerSecond);
}
