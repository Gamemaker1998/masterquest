package engine.physicEngine;

import static engine.Logic.between;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import engine.Engine;
import engine.data.Vector2D;
import engine.data.util.vector.Vector2f;
import engine.data.util.vector.Vector3f;
import entity.Entity;


public final class PhysicManager
{
	private class PhysicsChunk
	{
		//TODO: Physics components do not get moved to a different physic chunk but they belong in the same 
		private ArrayList<PhysicsComponent> physicsComponents;

		public PhysicsChunk()
		{
			physicsComponents = new ArrayList<>();
		}
	}

	private static Map<Vector2D, PhysicsChunk> physicsChunks;
	private static final int physicsChunkDistance = 1000;
	private static PhysicsChunk currentPhysicsChunk;

	private static final Vector3f gravity = new Vector3f(0, -50.0f, 0);

	public PhysicManager()
	{
		physicsChunks = new HashMap<>();
	}

	public static boolean translate(Vector3f position, Vector3f adder)
	{
		position.translate(position.x, position.y, position.z);
		return true;
	}
	
	public boolean translate(Entity entity, Vector3f adder)
	{
		/* The position must be replaced by an entity or at least a physics component.
		 * The aim of this is to say that just a position cannot be related to physic characteristics as physics components do.
		 * So if it is a physics component it will go through the collision detection otherwise, in case of a position, it will
		 * just have added adder to the position.
		 * If it is an entity there will be checked if it has an own physics component.
		 * An entity without a physics component will just have added adder to its position.
		*/
		if(entity.getPhysicsComponent() != null)
		{
			return translate(entity.getPhysicsComponent(), adder);
		}
		else
		{
			return translate(entity.getTransform().getPosition(), adder);
		}
	}
	
	public static boolean translate(PhysicsComponent physicsComponent, Vector3f adder)
	{
		// TODO: Collision detection. The speed of the physics component must be set to 0 if it collided with another physics component
		// Doesn't work completely fine
		Vector3f position = physicsComponent.getTransform().getPosition();
		boolean b1 = false, b2 = false;
		
		if(!physicsComponent.isKinetic())
		{
			Vector3f dimension = physicsComponent.getTransform().getDimension();
			PhysicsChunk physicsChunk = physicsChunks.get(calculatePhysicsChunkPosition(position));
			Vector3f manipulatedAdder = new Vector3f(0.0f, 0.0f, 0.0f);
			if(physicsChunk != null)
			{
				for(PhysicsComponent pc: physicsChunk.physicsComponents)
				{
					if(pc.isKinetic())
						continue;
					if(pc == physicsComponent)
						continue;
					//Here it is checked if a physics component collides with another physic component
					if((b1=between(position.x+adder.x-dimension.x/2, dimension.x, pc.getTransform().getPosition().x-pc.getTransform().getDimension().x/2, pc.getTransform().getDimension().x))
							&&
					   (b2=between(position.y+adder.y-dimension.y/2, dimension.y, pc.getTransform().getPosition().y-pc.getTransform().getDimension().y/2, pc.getTransform().getDimension().y)))
					{
						float difference = 0;
						boolean collision = false;
						//Here the difference of the physics component compared to the other physics component will be calculated and added
						if(Math.abs(adder.x) > 0 && Math.abs(adder.x) > (difference = Math.abs(pc.getTransform().getPosition().x-position.x)))
						{
							manipulatedAdder.x = difference;
						}
						if(difference != 0 && adder.x != 0)
						{
							physicsComponent.getSpeed().x = 0;
							physicsComponent.setSideCollision(true);
						}
						if(adder.y != 0 && Math.abs(adder.y) > (difference = Math.abs((pc.getTransform().getPosition().y-pc.getTransform().getDimension().y/2)-(position.y+dimension.y/2))))
						{
							manipulatedAdder.y = difference;
						}
						//if(physicsComponent.s.equals("test"))System.out.println(Math.abs(adder.y) + " " + (difference = (Math.abs(pc.getTransform().getPosition().y-position.y)-(pc.getTransform().getDimension().y/2.0+dimension.y/2.0))));
						if(difference != 0 && adder.y != 0)
						{						
							//if(physicsComponent.s.equals("test"))System.out.println("y " + difference + " " + (position.y-pc.getTransform().getPosition().y) + "mani:" + manipulatedAdder.y + " " + adder.y);
							physicsComponent.getSpeed().y = 0;
							physicsComponent.setGroundCollision(true);
						}
						
						collision = true;
						position.translate(manipulatedAdder.x, manipulatedAdder.y, manipulatedAdder.z);
						if(collision)
						{
							pc.onCollision();
						}
						return false;
					}
				}
			}
		}
		position.translate(adder.x, adder.y, adder.z);
		return true;
	}
	
	
	/** Calculates whether there has been a collision between two physics components
	 * 
	 */
	public boolean collision(PhysicsComponent physicsComponent1, PhysicsComponent physicsComponent2)
	{
		return false;
	}

	/**
	 * Calculates the physics of the physicsComponents like the gravity and the
	 * forces which where be added
	 */
	public void physics()
	{
		/* TODO: Gravity is not equal on every entity 
		 * If we have a acceleration of (4.0f, 0.0f) it means that the object has to be accelerated
		 * to the right.
		 * The acceleration is given as u/s� which means that in one second the speed has to be increased
		 * by 4.0f units per second
		 */
		/*for(double y=Entity.mainCameraPosition.y-(Entity.mainCameraPosition.y%physicsChunkDistance)-physicsChunkDistance, yy=-physicsChunkDistance; yy<=physicsChunkDistance; y+=physicsChunkDistance, yy+=physicsChunkDistance)
		{
			for(double x=Entity.mainCameraPosition.x-(Entity.mainCameraPosition.x%physicsChunkDistance)-physicsChunkDistance, xx=-physicsChunkDistance; xx<=physicsChunkDistance; x+=physicsChunkDistance, xx+=physicsChunkDistance)
			{
				if(physicsChunks.get(new Vector2D(x, y)) == null)
					continue;
				currentPhysicsChunk = physicsChunks.get(new Vector2D(x, y));
				for(PhysicsComponent pc: currentPhysicsChunk.physicsComponents)
				{
					if(pc == null || !pc.isGravity())
						continue;
					Vector2f speed = pc.getSpeed();
					speed.translate((float) (gravity.x * Engine.getDeltaTime()), (float)(gravity.y * Engine.getDeltaTime()));
					//System.out.println(speed.x + " " + speed.y);
					translate(pc, new Vector3f(speed.x * Engine.getDeltaTime(), speed.y * Engine.getDeltaTime(), 0));
				}
			}
		}*/
	}

	public void addPhysicsComponent(PhysicsComponent physicsComponent)
	{
		PhysicsChunk physicsChunk = physicsChunks.get(calculatePhysicsChunkPosition(physicsComponent.getTransform().getPosition()));
		if(physicsChunk == null)
		{
			physicsChunk = new PhysicsChunk();
			physicsChunks.put(calculatePhysicsChunkPosition(physicsComponent.getTransform().getPosition()), physicsChunk);
		}
		physicsChunk.physicsComponents.add(physicsComponent);
	}

	public void removePhysicsComponent(PhysicsComponent physicsComponent)
	{
		physicsChunks.get(calculatePhysicsChunkPosition(physicsComponent.getTransform().getPosition())).physicsComponents.remove(physicsComponent);
	}

	private static Vector2D calculatePhysicsChunkPosition(Vector3f position)
	{
		return new Vector2D(position.x - position.x % physicsChunkDistance, position.y - position.y % physicsChunkDistance);
	}

	private static Vector2D calculatePhysicsChunkPosition(double y, double x)
	{
		return new Vector2D(x - x % physicsChunkDistance, y - y % physicsChunkDistance);
	}
}
