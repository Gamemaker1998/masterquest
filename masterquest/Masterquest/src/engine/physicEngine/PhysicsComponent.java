package engine.physicEngine;

import engine.data.Transform;
import engine.data.Vector2D;
import engine.data.util.vector.Vector2f;

public class PhysicsComponent
{
	private Transform transform;
	private Vector2f acceleration;
	private Vector2f speed;
	private boolean isKinetic;
	private boolean gravity;
	public String s;
	private boolean groundCollision;
	private boolean sideCollision;

	public PhysicsComponent(Transform transform)
	{
		this.transform = transform;
		this.acceleration = new Vector2f(0.0f, 0.0f);
		this.speed = new Vector2f(0.0f, 0.0f);
		isKinetic = false;
		gravity = false;
		groundCollision = false;
		sideCollision = false;
	}

	public PhysicsComponent(PhysicsComponent physicsComponent)
	{
		this.transform = physicsComponent.transform;
		this.acceleration = physicsComponent.acceleration;
		this.speed = physicsComponent.acceleration;
		this.isKinetic = physicsComponent.isKinetic;
		this.gravity = physicsComponent.gravity;
	}

	public static PhysicsComponent createPhysicsComponent(Transform transform)
	{
		return new PhysicsComponent(transform);
	}

	public void setTransform(Transform transform)
	{
		this.transform = transform;
	}

	public void setAcceleration(Vector2f acceleration)
	{
		this.acceleration = acceleration;
	}

	public void setSpeed(Vector2f speed)
	{
		this.speed = speed;
	}

	public PhysicsComponent setKinetic(boolean isKinetic)
	{
		this.isKinetic = isKinetic;
		return this;
	}

	public PhysicsComponent setGravity(boolean gravity)
	{
		this.gravity = gravity;
		return this;
	}

	public PhysicsComponent setGroundCollision(boolean groundCollision)
	{
		this.groundCollision = groundCollision;
		return this;
	}

	public PhysicsComponent setSideCollision(boolean sideCollision)
	{
		this.sideCollision = sideCollision;
		return this;
	}

	public Transform getTransform()
	{
		return transform;
	}

	public Vector2f getAcceleration()
	{
		return acceleration;
	}

	public Vector2f getSpeed()
	{
		return speed;
	}

	public boolean isKinetic()
	{
		return isKinetic;
	}

	public boolean isGravity()
	{
		return gravity;
	}

	public boolean isGroundCollision()
	{
		return groundCollision;
	}

	@Override
	public int hashCode()
	{
		return 1;
	}

	/*public boolean equals(Object o)
	{
		if(o == null)
			return false;
		if(o instanceof PhysicsComponent)
		{
			PhysicsComponent pc = (PhysicsComponent)(o);
			return 
		}
		return;
	}*/

	// Methods which are called by the Physic Manager

	public void onCollision()
	{

	}
}
