package engine.data;

import java.util.HashMap;
import java.util.Map;

import engine.Engine;
import entity.Entity;
import manager.DataManager.ClassGroup;


/**
 * ClassStore is a business logic class which can be used to configure data
 * groups and get special sets of data.
 * 
 * @author yan
 *
 */
public class ClassGroupPointer
{
	public interface ClassGroupRequest
	{
		public void manipulateClassGroup(Entity entity);
	}

	/**
	 * ClassGroupIDs: Very ecessary to use since the IDs could change because of the need of optimising and improving the engine
	 */
	public static final long MASTER_CLASS_ID = Integer.MAX_VALUE;
	public static final long EMPTY_ENTITY_CLASS_ID = Integer.MIN_VALUE;
	public static final long DEFAULT_CLASS_ID = 0;
	public static final long ACTION_ZONE_CLASS_ID = 1;
	public static final long SOUND_SOURCE_CLASS_ID = 2;
	public static final long FLUIDS_CLASS_ID = 3;
	public static final long GUI_COMPONENTS_CLASS_ID = 4;
	public static final long BACKGROUND_CLASS_ID = 5;
	
	// ClassGroupNames
	public static final String MASTER_CLASS_NAME = "master_class";
	public static final String DEFAULT_CLASS_NAME = "default_class";
	public static final String ACTION_ZONE_CLASS_NAME = "action_zone";
	public static final String SOUND_SOURCE_CLASS_NAME = "sound_source";
	public static final String FLUIDS_CLASS_NAME = "fluids";
	public static final String GUI_COMPONENTS_NAME = "gui_components";
	public static final String BACKGROUND_NAME = "background";

	private Map<Long, ClassGroup> selectedClassGroups;
	private ClassGroupRequest usedClassGroupRequest;

	public ClassGroupPointer()
	{
		selectedClassGroups = new HashMap<>();
		usedClassGroupRequest = null;
	}

	public void selectClassGroup(long... classIDs)
	{/*
		ClassGroup currentClassGroup;
		for(Long classID: classIDs)
		{
			if((currentClassGroup = Engine.dataManager.getClassGroup(classID)) != null)
			{
				selectedClassGroups.put(classID, currentClassGroup);
			}
		}*/
	}

	public void removeClassGroup(long... classIDs)
	{
		for(Long classID: classIDs)
		{
			selectedClassGroups.remove(classID);
		}
	}

	public void undoSelection()
	{
		selectedClassGroups.clear();
	}

	public void defineClassGroupRequest(ClassGroupRequest request)
	{
		usedClassGroupRequest = request;
	}

	public void runRequest()
	{
		if(usedClassGroupRequest != null)
		{
			for(ClassGroup classGroup: selectedClassGroups.values())
			{
				for(Entity entity: classGroup.getEntities())
					usedClassGroupRequest.manipulateClassGroup(entity);
			}
		}
	}
}
