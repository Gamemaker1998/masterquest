package engine.data;

public class Range3D
{
	private double xRange;
	private double yRange;
	private double zRange;
	
	public Range3D(double xRange, double yRange, double zRange)
	{
		this.xRange = xRange;
		this.yRange = yRange;
		this.zRange = zRange;
	}
	
	public double getXRange()
	{
		return xRange;
	}
	
	public double getYRange()
	{
		return yRange;
	}
	
	public double getZRange()
	{
		return zRange;
	}
}
