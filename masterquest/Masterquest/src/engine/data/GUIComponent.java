package engine.data;

import engine.Engine;
import engine.renderEngine.shaderEngine.ShaderModel;


public class GUIComponent
{
	private Transform position;
	private boolean visible;
	private ShaderModel shaderModel;
	private ShaderModel hoverShaderModel;
	
	
	public GUIComponent(Transform position, ShaderModel shaderModel)
	{
		this.position = position;
		this.shaderModel = shaderModel;
	}
	
	
	public GUIComponent setVisible(boolean visible)
	{
		this.visible = visible;
		if(visible && shaderModel!=null)
			Engine.dataManager.addGUIComponent(this);
		else
			Engine.dataManager.removeGUIComponent(this);
		return this;
	}
	
	public GUIComponent addShaderModel(ShaderModel shaderModel)
	{
		this.shaderModel = shaderModel;
		return this;
	}
	
	public GUIComponent addHoverShaderModel(ShaderModel hoverShaderModel)
	{
		this.hoverShaderModel = hoverShaderModel;
		return this;
	}
}
