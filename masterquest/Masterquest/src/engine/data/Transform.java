package engine.data;

import engine.data.util.vector.Vector3f;

public class Transform
{
	protected Vector3f position;
	// Optional
	protected Vector3f dimension;
	private Vector3f rotation; // Rotation in radiants

	public Transform()
	{
		this(new Vector3f(), null, null);
	}
	
	public Transform(Vector3f position)
	{
		this(position, new Vector3f(1, 1, 1), new Vector3f(0, 0, 0));
	}

	public Transform(Vector3f position, Vector3f dimension, Vector3f rotation)
	{
		this.position = position;
		this.dimension = dimension;
		this.rotation = rotation;
	}
	
	// Copy-constructor
	public Transform(Transform transform)
	{
		this(transform.position, transform.dimension, transform.rotation);
	}

	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	public void setDimension(Vector3f dimension)
	{
		this.dimension = dimension;
	}

	public void setRotation(Vector3f rotation)
	{
		this.rotation = rotation;
	}

	public Vector3f getPosition()
	{
		return position;
	}

	public Vector3f getDimension()
	{
		return dimension;
	}

	public Vector3f getRotation()
	{
		return rotation;
	}
}
