package engine.physicEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import engine.Engine;
import engine.data.Entity;
import engine.data.PhysicsComponent;
import engine.data.Vector2D;
import engine.data.Vector3D;

import static engine.Logic.*;


public final class PhysicManager
{
	private class PhysicsChunk
	{
		//TODO: Physics components do not get moved to a different physic chunk but they belong in the same 
		private ArrayList<PhysicsComponent> physicsComponents;

		public PhysicsChunk()
		{
			physicsComponents = new ArrayList<>();
		}
	}

	private static Map<Vector2D, PhysicsChunk> physicsChunks;
	private static final int physicsChunkDistance = 1000;
	private static PhysicsChunk currentPhysicsChunk;

	private static final Vector3D gravity = new Vector3D(0, -50.0f, 0);

	public PhysicManager()
	{
		physicsChunks = new HashMap<>();
	}

	public static boolean translate(Vector3D position, Vector3D adder)
	{
		position.add(adder);
		return true;
	}
	
	public boolean translate(Entity entity, Vector3D adder)
	{
		/* The position must be replaced by an entity or at least a physics component.
		 * The aim of this is to say that just a position cannot be related to physic characteristics as physics components do.
		 * So if it is a physics component it will go through the collision detection otherwise, in case of a position, it will
		 * just have added adder to the position.
		 * If it is an entity there will be checked if it has an own physics component.
		 * An entity without a physics component will just have added adder to its position.
		*/
		if(entity.getPhysicsComponent() != null)
		{
			return translate(entity.getPhysicsComponent(), adder);
		}
		else
		{
			return translate(entity.getTransform().getPosition(), adder);
		}
	}
	
	public static boolean translate(PhysicsComponent physicsComponent, Vector3D adder)
	{
		// TODO: Collision detection. The speed of the physics component must be set to 0 if it collided with another physics component
		// Doesn't work completely fine
		Vector3D position = physicsComponent.getTransform().getPosition();
		boolean b1 = false, b2 = false;
		
		if(!physicsComponent.isKinetic())
		{
			Vector3D dimension = physicsComponent.getTransform().getDimension();
			PhysicsChunk physicsChunk = physicsChunks.get(calculatePhysicsChunkPosition(position));
			Vector3D manipulatedAdder = new Vector3D(0.0f, 0.0f, 0.0f);
			if(physicsChunk != null)
			{
				for(PhysicsComponent pc: physicsChunk.physicsComponents)
				{
					if(pc.isKinetic())
						continue;
					if(pc == physicsComponent)
						continue;
					//Here it is checked if a physics component collides with another physic component
					if((b1=between(position.xCoord+adder.xCoord-dimension.xCoord/2, dimension.xCoord, pc.getTransform().getPosition().xCoord-pc.getTransform().getDimension().xCoord/2, pc.getTransform().getDimension().xCoord))
							&&
					   (b2=between(position.yCoord+adder.yCoord-dimension.yCoord/2, dimension.yCoord, pc.getTransform().getPosition().yCoord-pc.getTransform().getDimension().yCoord/2, pc.getTransform().getDimension().yCoord)))
					{
						double difference = 0;
						boolean collision = false;
						//Here the difference of the physics component compared to the other physics component will be calculated and added
						if(Math.abs(adder.xCoord) > 0 && Math.abs(adder.xCoord) > (difference = Math.abs(pc.getTransform().getPosition().xCoord-position.xCoord)))
						{
							manipulatedAdder.xCoord = difference;
						}
						if(difference != 0 && adder.xCoord != 0)
						{
							physicsComponent.getSpeed().xCoord = 0;
							physicsComponent.setSideCollision(true);
						}
						if(adder.yCoord != 0 && Math.abs(adder.yCoord) > (difference = Math.abs((pc.getTransform().getPosition().yCoord-pc.getTransform().getDimension().yCoord/2)-(position.yCoord+dimension.yCoord/2))))
						{
							manipulatedAdder.yCoord = difference;
						}
						//if(physicsComponent.s.equals("test"))System.out.println(Math.abs(adder.yCoord) + " " + (difference = (Math.abs(pc.getTransform().getPosition().yCoord-position.yCoord)-(pc.getTransform().getDimension().yCoord/2.0+dimension.yCoord/2.0))));
						if(difference != 0 && adder.yCoord != 0)
						{						
							//if(physicsComponent.s.equals("test"))System.out.println("y " + difference + " " + (position.yCoord-pc.getTransform().getPosition().yCoord) + "mani:" + manipulatedAdder.yCoord + " " + adder.yCoord);
							physicsComponent.getSpeed().yCoord = 0;
							physicsComponent.setGroundCollision(true);
						}
						
						collision = true;
						position.add(manipulatedAdder);
						if(collision)
						{
							pc.onCollision();
						}
						return false;
					}
				}
			}
		}
		position.add(adder);
		return true;
	}
	
	
	/** Calculates whether there has been a collision between two physics components
	 * 
	 */
	public boolean collision(PhysicsComponent physicsComponent1, PhysicsComponent physicsComponent2)
	{
		return false;
	}

	/**
	 * Calculates the physics of the physicsComponents like the gravity and the
	 * forces which where be added
	 */
	public void physics()
	{
		/* TODO: Gravity is not equal on every entity 
		 * If we have a acceleration of (4.0f, 0.0f) it means that the object has to be accelerated
		 * to the right.
		 * The acceleration is given as u/s� which means that in one second the speed has to be increased
		 * by 4.0f units per second
		 */
		for(double y=Entity.mainCameraPosition.yCoord-(Entity.mainCameraPosition.yCoord%physicsChunkDistance)-physicsChunkDistance, yy=-physicsChunkDistance; yy<=physicsChunkDistance; y+=physicsChunkDistance, yy+=physicsChunkDistance)
		{
			for(double x=Entity.mainCameraPosition.xCoord-(Entity.mainCameraPosition.xCoord%physicsChunkDistance)-physicsChunkDistance, xx=-physicsChunkDistance; xx<=physicsChunkDistance; x+=physicsChunkDistance, xx+=physicsChunkDistance)
			{
				if(physicsChunks.get(new Vector2D(x, y)) == null)
					continue;
				currentPhysicsChunk = physicsChunks.get(new Vector2D(x, y));
				for(PhysicsComponent pc: currentPhysicsChunk.physicsComponents)
				{
					if(pc == null || !pc.isGravity())
						continue;
					Vector2D speed = pc.getSpeed();
					speed.add((float) (gravity.xCoord * Engine.getDeltaTime()), (float)(gravity.yCoord * Engine.getDeltaTime()));
					//System.out.println(speed.xCoord + " " + speed.yCoord);
					translate(pc, new Vector3D(speed.xCoord * Engine.getDeltaTime(), speed.yCoord * Engine.getDeltaTime(), 0));
				}
			}
		}
	}

	public void addPhysicsComponent(PhysicsComponent physicsComponent)
	{
		PhysicsChunk physicsChunk = physicsChunks.get(calculatePhysicsChunkPosition(physicsComponent.getTransform().getPosition()));
		if(physicsChunk == null)
		{
			physicsChunk = new PhysicsChunk();
			physicsChunks.put(calculatePhysicsChunkPosition(physicsComponent.getTransform().getPosition()), physicsChunk);
		}
		physicsChunk.physicsComponents.add(physicsComponent);
	}

	public void removePhysicsComponent(PhysicsComponent physicsComponent)
	{
		physicsChunks.get(calculatePhysicsChunkPosition(physicsComponent.getTransform().getPosition())).physicsComponents.remove(physicsComponent);
	}

	private static Vector2D calculatePhysicsChunkPosition(Vector3D position)
	{
		return new Vector2D(position.xCoord - position.xCoord % physicsChunkDistance, position.yCoord - position.yCoord % physicsChunkDistance);
	}

	private static Vector2D calculatePhysicsChunkPosition(double y, double x)
	{
		return new Vector2D(x - x % physicsChunkDistance, y - y % physicsChunkDistance);
	}
}
