package engine;

import java.util.LinkedList;


public abstract class Process implements Processable
{	
	protected static LinkedList<Process> processes;
	
	// Variablen auf die schnell zugegriffen werden muss
	private Process currentProcess;
	
	@Override
	public abstract void process();
	
	
	public static void addProcess(Process process)
	{
		addProcess(process, processes.size());
	}
	
	public static void addProcess(Process process, int processId)
	{
		processes.add(processId, process);
	}
	
	public boolean changeProcessId(int current, int updated)
	{
		currentProcess = processes.get(current);
		if(currentProcess == null)
			return false;
		processes.add(updated, currentProcess);
		processes.remove(current);
		return true;
	}
}
