package engine.renderEngine.shaderEngine.animation;

import java.util.ArrayList;
import java.util.Iterator;

import engine.renderEngine.TextureLegacy;


public class Animation
{
	public static class AnimationPoint
	{
		public TextureLegacy texture;
		public long milliseconds;

		public AnimationPoint(TextureLegacy texture, long milliseconds)
		{
			this.texture = texture;
			this.milliseconds = milliseconds;
		}

		public TextureLegacy getCurrentTexture()
		{
			return texture;
		}
	}

	private String name;
	// Characteristics of the animation
	private boolean loop;
	
	private boolean animate;
	private long deltaTime;
	private AnimationPoint currentAnimationPoint;
	// TODO: See how to set the entitie's texture to the default one
	private TextureLegacy currentTexture;

	private ArrayList<AnimationPoint> animationPoints;
	private Iterator<AnimationPoint> iterator;

	public Animation(String name)
	{
		this.name = name;
		animationPoints = new ArrayList<>();
		loop = false;
	}

	public void animate()
	{
		if(deltaTime == 0)
		{
			deltaTime = System.currentTimeMillis();
			iterator = animationPoints.iterator();
			currentAnimationPoint = iterator.next();
			currentTexture = currentAnimationPoint.texture;
		}
		if(System.currentTimeMillis() - deltaTime >= currentAnimationPoint.milliseconds)
		{
			if(iterator.hasNext())
			{
				currentAnimationPoint = iterator.next();
				currentTexture = currentAnimationPoint.texture;
				deltaTime = System.currentTimeMillis();
			}
			else
			{
				deltaTime = 0;
				if(!loop)
					animate = false;
			}
		}
	}

	public TextureLegacy getCurrentTexture()
	{
		return currentTexture;
	}

	public void addAnimationPoint(TextureLegacy texture, long milliseconds)
	{
		animationPoints.add(new AnimationPoint(texture, milliseconds));
	}

	public void addAnimationPoint(int id, TextureLegacy texture, long milliseconds)
	{
		animationPoints.add(id, new AnimationPoint(texture, milliseconds));
	}
	
	public Animation setLoop(boolean loop)
	{
		this.loop = loop;
		return this;
	}

	public String getName()
	{
		return name;
	}

	public boolean isAnimated()
	{
		return animate;
	}

	public void startAnimation()
	{
		animate = true;
	}

	public void stopAnimation()
	{
		animate = false;
	}
}
