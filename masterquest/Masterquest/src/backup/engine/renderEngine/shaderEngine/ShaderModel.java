package engine.renderEngine.shaderEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import engine.renderEngine.TextureLegacy;
import engine.renderEngine.shaderEngine.animation.Animation;


public class ShaderModel
{
	private int modelType;
	private ArrayList<TextureLegacy> textures;
	private Map<String, Animation> animations;
	private TextureLegacy defaultTexture;
	private TextureLegacy actualTexture;
	private Animation defaultAnimation;

	public static final int MODEL_TYPE_DEFAULT = 0;
	public static final int MODEL_TYPE_WATER = 1;
	public static final int MODEL_TYPE_SAND = 2;
	public static final int MODEL_TYPE_TREE = 3;

	public ShaderModel(TextureLegacy texture)
	{
		this(texture, MODEL_TYPE_DEFAULT);
	}

	public ShaderModel(TextureLegacy texture, int modelType)
	{
		textures = new ArrayList<>();
		animations = new HashMap<>();
		
		defaultTexture = texture;
		actualTexture = texture;
		
		textures.add(defaultTexture);
		this.modelType = modelType;
	}

	public void addTexture(TextureLegacy texture)
	{
		textures.add(texture);
	}

	public ShaderModel addAnimation(Animation animation)
	{
		if(defaultAnimation == null)
			defaultAnimation = animation;
		animations.put(animation.getName(), animation);
		return this;
	}

	public void setModelType(int modelType)
	{
		this.modelType = modelType;
	}

	public void setTexture(TextureLegacy texture)
	{
		this.actualTexture = texture;
	}
	
	public void setDefaultAnimation(Animation defaultAnimation)
	{
		this.defaultAnimation = defaultAnimation;
	}
	
	public TextureLegacy getDefaultTexture()
	{
		return defaultTexture;
	}

	public TextureLegacy getTexture()
	{
		return actualTexture;
	}
	
	public Map<String, Animation> getAnimations()
	{
		return animations;
	}
}
