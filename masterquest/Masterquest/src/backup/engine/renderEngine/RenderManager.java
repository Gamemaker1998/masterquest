package engine.renderEngine;

import java.util.ArrayList;
import java.util.LinkedList;

import engine.Engine;
import engine.Renderable;
import engine.data.Background;
import engine.data.Charakter;
import engine.data.Entity;
import engine.data.GUIComponent;
import engine.data.Vector2D;
import engine.data.Vector3D;
import engine.renderEngine.shaderEngine.ShaderManager;


public final class RenderManager
{
	private ShaderManager shaderManager;
	private ArrayList<Renderable> renderMethods;
	private Render render;
	public Screen screen;
	private Background defaultBackground;// = new Background(new Texture[]{Texture.loadBitmap("res\\textures\\Cobbelstone.png")});

	private int middleWidth;
	private int middleHeight;
	private float pixelsPerUnit;

	private int framesPerSecond;
	private int frames;
	private long deltaTime;
	private float rotation = 0;

	public static final float UNIT_POS = 0.05f; // UNIT_PERCENTAGE_OF_SCREEN 1 in-game unit is equal to 4% of the screen's width
	private static final Vector3D sightDistance = new Vector3D(15, 15, 0);

	private boolean[] renderingModes; // Enables the needed render options
	private int amountRenderingModes = 7; /* Currently 7 render modes
											* 0) The main renderer
											* 1) Only main renderer
											* 2) Entity renderer
											* 3) Charakter renderer
											* 4) GUI renderer
											* 5) Background renderer
											* 6) Rendering process renderer
											*/

	// Variablen auf die schnell zugegriffen werden muss
	private Vector3D currentPosition;
	private Vector3D currentDimension;
	private ArrayList<Entity> entities;
	private ArrayList<Charakter> charakters;

	public RenderManager()
	{
		shaderManager = new ShaderManager();
		renderMethods = new ArrayList<>();

		renderingModes = new boolean[amountRenderingModes];
		setRenderingModes(true, true, true, true, true, true, true);
	}

	// Main renderer
	public void render(int ticks)
	{
		if(renderingModes[0])
		{
			if(deltaTime == 0)
			{
				framesPerSecond = ticks;
				deltaTime = System.currentTimeMillis();
			}
			if(System.currentTimeMillis() - deltaTime >= 1000)
			{
				deltaTime = System.currentTimeMillis();
				framesPerSecond = frames;
				frames = 0;
				System.out.println(framesPerSecond + " FPS");
			}
			framesPerSecond = ticks;
			render.clearScreen();

			Engine.dataManager.updateData();

			if(renderingModes[1])
			{
				if(renderingModes[5])
					;
				renderBackground();
				if(renderingModes[2])
					renderEntities();
				if(renderingModes[3])
					renderCharakters();

				/*if(renderingModes[6])
				{
					for(Renderable renderable: renderMethods)
					{
						renderable.render(ticks);
					}
				}*/
				//renderGUIComponents();
				//renderWithRotation();
				//shaderManager.shade();
			}

			screen.render();
			frames++;
		}
	}

	private void renderWithRotation() // F�r Testzwecke
	{
		/*Texture texture = Texture.loadBitmap("C:\\Users\\yan\\Software\\Masterquest\\res\\textures\\Cobbelstone.png");
		if((rotation += 0.01) > 2 * Math.PI)
			rotation -= 2 * Math.PI;
		render.render(350, 250, 100, 100, rotation, 400, 200, texture);
		render.render(600, 300, 100, 100, rotation, 650, 350, texture);*/

		// Test TODO configure renderWithRotation() method  
		entities = Engine.dataManager.getEntities(Entity.mainCameraPosition, sightDistance, true);
		if(entities == null)
			return;
		for(Entity entity: entities)
		{
			if(entity == null)
				continue;

			currentPosition = entity.getTransform().getPosition();
			currentDimension = entity.getTransform().getDimension();

			if(currentPosition.xCoord + currentDimension.xCoord / 2 >= Engine.dataManager.protectedMainCameraPosition.xCoord - sightDistance.xCoord && currentPosition.xCoord <= Engine.dataManager.protectedMainCameraPosition.xCoord + sightDistance.xCoord && currentPosition.yCoord + currentDimension.yCoord / 2 >= Engine.dataManager.protectedMainCameraPosition.yCoord - sightDistance.yCoord && currentPosition.yCoord <= Engine.dataManager.protectedMainCameraPosition.yCoord + sightDistance.yCoord)
			{
				if(entity.getShaderModel() == null || entity.getShaderModel().getTexture() == null)
					continue;
				render.render((int) (middleWidth - ((Engine.dataManager.protectedMainCameraPosition.xCoord - currentPosition.xCoord + currentDimension.xCoord / 2) * pixelsPerUnit)), (int) (middleHeight + ((Engine.dataManager.protectedMainCameraPosition.yCoord - currentPosition.yCoord - currentDimension.yCoord / 2) * pixelsPerUnit)), currentDimension.getXCoord() * pixelsPerUnit, currentDimension.getYCoord() * pixelsPerUnit, entity.getTransform().getRotation().zCoord, (int) (middleWidth - (Engine.dataManager.protectedMainCameraPosition.xCoord - currentPosition.xCoord) * pixelsPerUnit), (int) (middleHeight + (Engine.dataManager.protectedMainCameraPosition.yCoord - currentPosition.yCoord) * pixelsPerUnit), entity.getShaderModel().getTexture());
			}
		}
	}

	private void renderEntities()
	{
		Engine.dataManager.setCurrentPosition(Engine.dataManager.protectedMainCameraPosition);
		Engine.dataManager.setSightDistance(sightDistance);
		entities = Engine.dataManager.getEntities(Engine.dataManager.protectedMainCameraPosition, sightDistance, true);
		if(entities == null)
			return;

		for(Entity entity: entities)
		{
			if(entity == null)
				continue;

			currentPosition = entity.getTransform().getPosition();
			currentDimension = entity.getTransform().getDimension();
			if(currentPosition.xCoord + currentDimension.xCoord / 2 >= Engine.dataManager.protectedMainCameraPosition.xCoord - sightDistance.xCoord && currentPosition.xCoord <= Engine.dataManager.protectedMainCameraPosition.xCoord + sightDistance.xCoord && currentPosition.yCoord + currentDimension.yCoord / 2 >= Engine.dataManager.protectedMainCameraPosition.yCoord - sightDistance.yCoord && currentPosition.yCoord <= Engine.dataManager.protectedMainCameraPosition.yCoord + sightDistance.yCoord)
			{
				if(entity.getShaderModel() == null || entity.getShaderModel().getTexture() == null)
					continue;
				render.render((int) (middleWidth - ((Engine.dataManager.protectedMainCameraPosition.xCoord - currentPosition.xCoord + currentDimension.xCoord / 2) * pixelsPerUnit)), (int) (middleHeight + ((Engine.dataManager.protectedMainCameraPosition.yCoord - currentPosition.yCoord - currentDimension.yCoord / 2) * pixelsPerUnit)), currentDimension.getXCoord() * pixelsPerUnit, currentDimension.getYCoord() * pixelsPerUnit, entity.getShaderModel().getTexture());
			}
		}
	}

	private void renderCharakters()
	{
		ArrayList<Charakter> characters = Engine.dataManager.getCharacters(Engine.dataManager.protectedMainCameraPosition, sightDistance, true);
		for(Charakter character: characters)
		{
			Vector3D position = character.getTransform().getPosition();
			Vector3D dimension = character.getTransform().getDimension();
			render.render((int) (middleWidth - ((Engine.dataManager.protectedMainCameraPosition.xCoord - position.xCoord + dimension.xCoord / 2) * pixelsPerUnit)), (int) (middleHeight + ((Engine.dataManager.protectedMainCameraPosition.yCoord - position.yCoord - dimension.yCoord / 2) * pixelsPerUnit)), dimension.xCoord * pixelsPerUnit, dimension.yCoord * pixelsPerUnit, character.getShaderModel().getTexture(), (character.getTransform().getRotation().yCoord == Math.PI), false);
		}
	}

	private void renderBackground()
	{
		ArrayList<Background> backgrounds = Engine.dataManager.getBackground(Engine.dataManager.protectedMainCameraPosition, sightDistance, true);
		for(Background background: backgrounds)
		{
			Vector2D position = background.getBackgroundRange().position;
			Vector2D dimension = background.getBackgroundRange().dimension;
			render.render((int) (middleWidth - ((Engine.dataManager.protectedMainCameraPosition.xCoord - position.xCoord + dimension.xCoord / 2) * pixelsPerUnit)), (int) (middleHeight + ((Engine.dataManager.protectedMainCameraPosition.yCoord - position.yCoord - dimension.yCoord / 2) * pixelsPerUnit)), dimension.xCoord * pixelsPerUnit, dimension.yCoord * pixelsPerUnit, background.getTexture());
		}
	}

	private void renderGUIComponents()
	{
		//TODO configure renderGUIComponents() method and the necessary algorithm
		for(GUIComponent component: Engine.dataManager.getGUIComponents())
		{
			render.render((int)(middleHeight-currentPosition.getYCoord()*pixelsPerUnit), (int)(middleWidth-currentPosition.getXCoord()*pixelsPerUnit), guiComponent.getShaderModel().getTexture()););
		}
	}

	public void setRender(Render render)
	{
		this.render = render;
		middleWidth = render.getWidth() >> 1;
		middleHeight = render.getHeight() >> 1;
		pixelsPerUnit = render.getWidth() * UNIT_POS;
		calculateOptimalSightDistance();
	}

	public void createScreen(String title, int width, int height)
	{
		screen = Screen.createScreen(title, width, height);
		screen.setInputHandler(Engine.inputHandler);
		setRender(new Render(width, height));
		screen.setRender(render);
	}

	/**
	 * Currently 7 render modes 0) The main renderer 1) Only main renderer 2)
	 * Entity renderer 3) Charakter renderer 4) GUI renderer 5) Background
	 * renderer 6) Rendering process renderer
	 */
	public void setRenderingModes(boolean... modes)
	{
		for(int i = 0; i < modes.length && i < amountRenderingModes; i++)
		{
			renderingModes[i] = modes[i];
		}
	}

	/**
	 * Returns the optimal sight distance saying that you just can see the last
	 * objects on the left and the right side
	 */
	public void calculateOptimalSightDistance()
	{
		int distance = (int) (800 / pixelsPerUnit);
		sightDistance.setVector3D(distance, distance, distance);
	}

	public int getFramesPerSecond()
	{
		return framesPerSecond;
	}

	public void setBackground(Background background)
	{
		this.defaultBackground = background;
	}
}
