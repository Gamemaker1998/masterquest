package engine.renderEngine;

import java.awt.image.*;
import java.io.*;
import javax.imageio.*;


public class Texture 
{
	public int width;
	public int height;
	public int[] pixels;
	
	
	public Texture(int width, int height)
	{
		this(width, height, new int[width * height]);
	}
	
	public Texture(int width, int height, int[] pixels)
	{
		this.width = width;
		this.height = height;
		this.pixels = pixels;
	}
	 	
	
	public static Texture loadBitmap(String fileName)
	{	
		try
		{
			BufferedImage image = ImageIO.read(new File(fileName));
			int width = image.getWidth();
			int height = image.getHeight();
			Texture texture = new Texture(width, height);
			image.getRGB(0, 0, width, height, texture.pixels, 0, width);
			return texture;
		}
		
		catch(IOException e)
		{
			System.err.println("CRASH");
			throw new RuntimeException(e);
		}
	}
	
	
	
	
	
	public static Texture loadSpecialBitmap(String datei, int x, int y, int endX, int endY)
	{
			BufferedImage IMAGE = null;
			
			try
			{
				IMAGE = ImageIO.read(new File(datei));
			}
			
			catch(IOException e)
			{
				return null;
			}
			
			int width = IMAGE.getWidth();
			int height = IMAGE.getHeight();
			Texture tex = new Texture(width, height);
			Texture texture = new Texture(endX, endY);
			BufferedImage image = new BufferedImage(endX, endX, BufferedImage.TYPE_INT_ARGB);

	        IMAGE.getRGB(x, y, endX, endY, tex.pixels, 0, endX);
	        image.setRGB(0, 0, endX, endY, tex.pixels, 0, endX);
	        image.getRGB(0, 0, endX, endY, texture.pixels, 0, endX);
		
			return texture;		
	}
	
	
	
	
	public int[] skalePixels(int skaleY, int skaleX)
	{
		double skaleFactorY = (double)(skaleY) / this.height;
		double skaleFactorX = (double)(skaleX) / this.width;
		int[] pixels = new int[(int)(skaleFactorY * this.height * skaleFactorX * this.width)];
		
		for(int y=0;  y<this.height * skaleFactorY;  y++)
		{
			for(int x=0;  x<this.width * skaleFactorX;  x++)
			{
				pixels[(int)(x + y * this.width * skaleFactorX)] = this.pixels[(int)((x * skaleFactorX) + (y * skaleFactorY) * this.width)];
			}
		}
		
		return pixels;
	}
	
	
	
	public Texture skaleTexture(int skaleY, int skaleX)
	{
		double skaleMultiplicatorY = this.height / (double)(skaleY);
		double skaleMultiplicatorX = this.width / (double)(skaleX);
		Texture texture = new Texture((int)(this.width / skaleMultiplicatorX), (int)(this.height / skaleMultiplicatorY));
		
		for(int y=0;  y<texture.height;  y++)
		{
			for(int x=0;  x<texture.width;  x++)
			{
				texture.pixels[x + y * texture.width] = this.pixels[(int)(x * skaleMultiplicatorX) + (int)(y * skaleMultiplicatorY) * this.width];
			}
		}
		return texture;
	}
	
	
	
	public static void skaleTexture(int skaleY, int skaleX, Texture[] texture)
	{
		Texture textureErsatz;
		for(int i=0;  i<texture.length;  i++)
		{
			double skaleMultiplicatorY = texture[i].height / (double)(skaleY);
			double skaleMultiplicatorX = texture[i].width / (double)(skaleX);	
			textureErsatz = new Texture((int)(texture[i].width / skaleMultiplicatorX), (int)(texture[i].height / skaleMultiplicatorY));
			for(int y=0;  y<textureErsatz.height;  y++)
			{
				for(int x=0;  x<textureErsatz.width;  x++)
				{
					textureErsatz.pixels[x + y * textureErsatz.width] = texture[i].pixels[(int)(x * skaleMultiplicatorX) + (int)(y * skaleMultiplicatorY) * texture[i].width];
				}
			}
			texture[i] = textureErsatz;
		}
	}
	
	
	
	
	public void setPixels(int yPos, int xPos, int yMaxPos, int xMaxPos, int width, int[] pixels)
	{			
		for(int y=yPos, yy=0;  yy<yMaxPos;  y++, yy++)
		{
			for(int x=xPos, xx=0;  xx<xMaxPos;  x++, xx++)
			{		
				this.pixels[x + y * this.width] = 1 + pixels[xx + yy * width];
			}
		}
	}
	
	
	
	////////////GETTER/SETTER METHODEN
	
	
	
	public int getWidth()
	{
		return width;
	}
	
	
	public int getHeight()
	{
		return height;
	}
}
