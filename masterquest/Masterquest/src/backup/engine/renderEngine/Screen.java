package engine.renderEngine;

import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import engine.Renderable;
import engine.input.InputHandler;


public class Screen extends Canvas implements Renderable
{
	private BufferedImage bimg;
	private JFrame jFrame;
	private Render render;
	private InputHandler inputHandler;
	
	// Variablen auf die schnell zugegriffen werden muss
	private BufferStrategy bs;
	private Graphics g;
	
	
	public Screen(String title, int width, int height)
	{
		bimg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		jFrame = new JFrame(title);
		jFrame.add(this);
		jFrame.setSize(width, height);
		jFrame.setCursor(new Cursor(5));
		
		jFrame.setUndecorated(true);
		jFrame.setResizable(false);
		jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jFrame.setLocationRelativeTo(null);
		jFrame.setAutoRequestFocus(true);
	}
	
	
	
	@Override
	public void render()
	{
		bs = this.getBufferStrategy();
		if(bs == null)
		{
			createBufferStrategy(3);
			return;
		}
		g = bs.getDrawGraphics();
		g.drawImage(bimg, 0, 0, render.getWidth(), render.getHeight(), null);
		g.dispose();
		bs.show();
	}
	
	
	
	public static Screen createScreen(String title, int width, int height)
	{
		return new Screen(title, width, height);
	}
	
	
	public void setInputHandler(InputHandler inputHandler)
	{
		if(this.inputHandler != null)
		{
			removeKeyListener(this.inputHandler);
			removeMouseListener(this.inputHandler);
			removeMouseMotionListener(this.inputHandler);
		}
		this.inputHandler = inputHandler;
		addKeyListener(inputHandler);
		addMouseListener(inputHandler);
		addMouseMotionListener(inputHandler);
	}
	
	public void setRender(Render render)
	{
		this.render = render;
		render.pixels = ((DataBufferInt)bimg.getRaster().getDataBuffer()).getData();
	}
	
	@Override
	public void setVisible(boolean visible)
	{
		jFrame.setVisible(visible);
	}



	@Override
	public void render(int framesPerSecond)
	{
		
	}

}
