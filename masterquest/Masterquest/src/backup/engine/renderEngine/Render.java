package engine.renderEngine;


public class Render 
{
	private int width;
	private int height;
	private int area;
	public int[] pixels;

	private static final int WHITE = 0xffffffff;
	private static final int BLACK = 0xff000000;
	private static final int ALPHA = 0xff000000;
	private static final int NO_ALPHA = 0x0;
	
	
	public Render(int width, int height)
	{
		this.width = width;
		this.height = height;
		area = width * height;
		pixels = new int[area];
	}
	
	
	public void render(int yPos, int xPos, Texture texture)
	{
		int tile;
		int yTimesWidth;
		int yTimeTextureWidth;
		
		int yEnd = yPos + texture.height;
		int xEnd = xPos + texture.width;
				
		for(int y=yPos, yy=0;  y<yEnd;  y++, yy++)
		{
			yTimesWidth = y * width;
			yTimeTextureWidth = yy * texture.width;
			for(int x=xPos, xx=0;  x<xEnd;  x++, xx++)
			{	
				
				if(y >= height)break;
				if(x >= width)continue;
				if(x < 0)continue;
				if(y < 0)continue;
				if(xx < 0)continue;
				if(yy < 0)continue;
				
				tile = texture.pixels[xx + yTimeTextureWidth];
				
				if(tile != NO_ALPHA)
					pixels[x + yTimesWidth] = tile;
			}
		}
	}
	
	
	
	
	public void render(int xPos, int yPos, double skaleX, double skaleY, Texture texture, boolean mirrowX, boolean mirrowY)
	{	
		int startX = 0;
		
		int color;
		int yyTimesWidth;
		
		double xFactor = texture.width / skaleX;
		double yFactor = texture.height / skaleY;
	
		float xx = 0;
		float yy = 0;
		double xEndPos = xPos + skaleX;
		double yEndPos = yPos + skaleY;
		
		if(mirrowX)
		{
			startX = texture.width-1;
			xFactor *= -1;
		}
		
		for(int y=yPos;  y<yEndPos;  y++, yy+=yFactor)
		{
			if(y >= height || yy >= texture.height)return;
			if(y < 0)continue;
			
			yyTimesWidth = (int)(yy) * texture.width;
			xx = startX;
			
			for(int x=xPos;  x<xEndPos;  x++, xx+=xFactor)
			{
				if(x >= width || xx >= texture.width)continue;
				if(x < 0)continue;
				if(x >= width || xx >= texture.width)break;
				color = texture.pixels[(int)(xx) + yyTimesWidth];
				
				if(color < ALPHA || color > WHITE)
					continue;
				pixels[x + y * this.width] = color;
			}
		}
	}
	
	
	public void render(int xPos, int yPos, double skaleX, double skaleY, Texture texture)
	{
		int color;
		int yyTimesWidth;
		
		double xFactor = texture.width / skaleX;
		double yFactor = texture.height / skaleY;
	
		double xx = 0;
		double yy = 0;
		double xStartPos = xPos;
		double yStartPos = yPos;
		double xEndPos = xPos + skaleX;
		double yEndPos = yPos + skaleY;
		
		if(yStartPos < 0)
		{
			yy = yFactor * Math.abs(yPos);
			yStartPos = 0;
		}
		else if(yStartPos > height)
		{
			return;
		}
				
		for(int y=(int)(yStartPos);  y<yEndPos;  y++, yy+=yFactor)
		{
			yyTimesWidth = (int)(yy) * texture.width;
			xx = 0;
			if(xPos < 0)
			{
				xx = xFactor * Math.abs(xPos);
				xStartPos = 0;
			}
			
			for(int x=(int)(xStartPos);  x<xEndPos;  x++, xx+=xFactor)
			{
				if(y >= height || yy >= texture.height)return;
				if(x >= width || xx >= texture.width)break;
				if(x < 0)continue;
				if(y < 0) continue;
				color = texture.pixels[(int)(xx) + yyTimesWidth];
				
				if(color < ALPHA || color > WHITE)
					continue;
				pixels[x + y * this.width] = color;
			}
		}
	}
	
	
	
	public void render(int yPos, int xPos, int yMaxPos, int xMaxPos, int[] pixels, boolean mirrowY, boolean mirrowX)
	{
		int startY = 0;
		int startX = 0;
		int adderY = 1;
		int adderX = 1;
		
		if(mirrowY)
		{
			startY = yMaxPos;
			adderY = -1;
		}
		
		if(mirrowX)
		{
			startX = xMaxPos;
			adderX = -1;
		}
		
		int tile;
				
		for(int y=yPos, yy=startY;  yy<yMaxPos;  y++, yy+=adderY)
		{
			for(int x=xPos, xx=startX;  xx<xMaxPos;  x++, xx+=adderX)
			{	
				if(y >= height)continue;
				if(x >= width)continue;
				if(xx >= xMaxPos)continue;
				if(yy >= yMaxPos)continue;
				if(x < 0)continue;
				if(y < 0)continue;
				if(xx < 0)continue;
				if(yy < 0)continue;
				
				tile = pixels[xx + yy * xMaxPos];
				if(tile != NO_ALPHA)
					this.pixels[x + y * width] = tile;
			}
		}
	}
	
	
	public void render(int xPos, int yPos, double skaleX, double skaleY, double radiants, double rotationPointX, double rotationPointY, Texture texture)
	{	
		double y = yPos;
		double x = xPos;
		
		double startAlpha;
		double endAlpha;
		
		double yFactor = texture.height / skaleY;
		double xFactor = texture.width / skaleX;
		
		float yIndex = 0;
		float xIndex = 0;
		int yTile;
		
		double yDistance;
		double xDistance;
		
		double radius;
		
		int tile;
		int pixel;
		
		for(int yOffset=yPos;  yIndex<texture.height;  yIndex+=yFactor, yOffset++)
		{
			xIndex = 0;
			yTile = (int)(yIndex) * texture.width;
			for(int xOffset=xPos;  xIndex<texture.width;  xIndex+=xFactor, xOffset++)
			{ 
				yDistance = yOffset - rotationPointY;
				xDistance = xOffset - rotationPointX;
				
				radius = Math.sqrt(Math.pow(yDistance, 2) + Math.pow(xDistance, 2));
				
				startAlpha = Math.atan(yDistance/xDistance);
				if(xDistance < 0)
					startAlpha += Math.PI;
				endAlpha = startAlpha + radiants;
				
				x = radius * Math.cos(endAlpha);
				y = radius * Math.sin(endAlpha);
		
				pixel = (int)(rotationPointX + x) + (int)(rotationPointY + y) * width;
	
				if(pixel >= 0 && pixel < area)
				{
					tile = texture.pixels[(int)(xIndex) + yTile];
					if(tile != NO_ALPHA)
						pixels[pixel] = tile;
				}
			}
		}
	}
	
	
	public void clearScreen()
	{
		for(int i=0;  i<area;  i++)
		{
			pixels[i] = 0xff000000;
		}
	}
	
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}

}
