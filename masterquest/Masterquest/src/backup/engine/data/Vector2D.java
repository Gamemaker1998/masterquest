package engine.data;

public class Vector2D
{
	public double xCoord;
	public double yCoord;

	public static final Vector2D ZERO = new Vector2D(0, 0);
	public static final Vector2D ONE = new Vector2D(1, 1);

	public Vector2D()
	{
		this(0, 0);
	}

	public Vector2D(Vector2D vector2D)
	{
		this(vector2D.xCoord, vector2D.yCoord);
	}

	public Vector2D(double xCoord, double yCoord)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}

	@Override
	public int hashCode()
	{
		return (int)(xCoord / yCoord);
	}

	@Override
	public boolean equals(Object object)
	{
		if(object == null)
			return false;
		if(object instanceof Vector2D)
		{
			Vector2D vector2D = (Vector2D) (object);
			return ((yCoord == vector2D.yCoord && xCoord == vector2D.xCoord));
		}
		return false;
	}

	public Vector2D add(Vector2D adder)
	{
		xCoord += adder.xCoord;
		yCoord += adder.yCoord;
		return this;
	}

	public Vector2D add(float x, float y)
	{
		xCoord += x;
		yCoord += y;
		return this;
	}

	public Vector2D div(double adder)
	{
		xCoord /= adder;
		yCoord /= adder;
		return this;
	}

	public Vector2D sub(Vector2D sub)
	{
		xCoord -= sub.xCoord;
		yCoord -= sub.yCoord;
		return this;
	}

	public Vector2D mul(Vector2D mul)
	{
		xCoord *= mul.xCoord;
		yCoord *= mul.yCoord;
		return this;
	}

	public Vector2D div(Vector2D div)
	{
		xCoord /= div.xCoord;
		yCoord /= div.yCoord;
		return this;
	}

	public Vector2D setVector2D(double x, double y)
	{
		this.xCoord = x;
		this.yCoord = y;
		return this;
	}

	public double getYCoord()
	{
		return yCoord;
	}

	public double getXCoord()
	{
		return xCoord;
	}

}
