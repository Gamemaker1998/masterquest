package engine.data;

import engine.Engine;
import engine.renderEngine.shaderEngine.ShaderModel;


public class Entity
{
	// Components
	protected long ID;
	protected String name;
	protected long classID;
	protected Transform transform;
	protected ShaderModel shaderModel;
	// Optional
	protected PhysicsComponent physicsComponent;

	/**
	 * The main camera position is the main view of the user. The whole engine
	 * rendering is based on that position which surely can be changed.
	 */
	public static Vector3D mainCameraPosition = new Vector3D(0.0f, 5.0f, 0.0f);// = new Vector3D(463.0f, 360.0f, 0); // Main-Camera

	public Entity(Transform transform)
	{
		this(Engine.dataManager.generateEntityID(), "-", ClassGroupPointer.DEFAULT_CLASS_ID, transform);
	}

	private Entity(long ID, String name, long classID, Transform transform)
	{
		this.name = name;
		this.classID = classID;
		this.transform = transform;
	}

	public Entity(Entity entity)
	{
		this(entity.ID, entity.name, entity.classID, entity.transform);
	}

	public static Entity createEntity(Transform transform)
	{
		Entity currentEntity = new Entity(transform);
		Engine.dataManager.addEntity(currentEntity);
		return currentEntity;
	}

	/**
	 * Every entity has its own run method which is run by the Engine
	 */
	public void run()
	{

	}

	public Entity addTransform(Transform transform) // Add components
	{
		this.transform = transform;
		return this;
	}

	public Entity addShaderModel(ShaderModel shaderModel)
	{
		this.shaderModel = shaderModel;
		return this;
	}

	/**
	 * Adds a new physics component to the object meaning that the object gets
	 * its own box collider (if isKinetic is false) and it can handle gravity
	 * (if gravity is true) It automatically adds the physics component to the
	 * physic manager too.
	 * 
	 * @return
	 */
	public Entity addPhysicsComponent()
	{
		this.physicsComponent = new PhysicsComponent(transform);
		Engine.physicManager.addPhysicsComponent(physicsComponent);
		return this;
	}

	public Entity groupEntity(long classID)
	{
		//TODO: Entity groupEntity: The entity shall be put into a class group
		this.classID = classID;
		return this;
	}

	public void setTransform(Transform transform)
	{
		this.transform = transform;
	}

	public void setShaderModel(ShaderModel shaderModel)
	{
		this.shaderModel = shaderModel;
	}

	public Transform getTransform()
	{
		return transform;
	}

	public ShaderModel getShaderModel()
	{
		return shaderModel;
	}

	public PhysicsComponent getPhysicsComponent()
	{
		return physicsComponent;
	}
	
	public long getClassID()
	{
		return classID;
	}
}
