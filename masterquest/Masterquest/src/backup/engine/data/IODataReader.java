package engine.data;

public interface IODataReader
{
	public Object read(String string);
}
