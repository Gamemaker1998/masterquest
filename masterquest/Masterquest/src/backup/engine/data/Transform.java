package engine.data;

public class Transform
{
	protected Vector3D position;
	protected Vector3D dimension;
	private Vector3D rotation; // Rotation in radiants

	public Transform()
	{
		this(new Vector3D(), new Vector3D(1, 1, 1), new Vector3D());
	}

	public Transform(Vector3D position, Vector3D dimension, Vector3D rotation)
	{
		this.position = position;
		this.dimension = dimension;
		this.rotation = rotation;
	}

	public void setPosition(Vector3D position)
	{
		this.position = position;
	}

	public void setDimension(Vector3D dimension)
	{
		this.dimension = dimension;
	}

	public void setRotation(Vector3D rotation)
	{
		this.rotation = rotation;
	}

	public Vector3D getPosition()
	{
		return position;
	}

	public Vector3D getDimension()
	{
		return dimension;
	}

	public Vector3D getRotation()
	{
		return rotation;
	}
}
