package engine.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import engine.Engine;
import engine.DataManager.ClassGroup;


/**
 * ClassStore is a business logic class which can be used to configure data
 * groups and get special sets of data.
 * 
 * @author yan
 *
 */
public class ClassGroupPointer
{
	public interface ClassGroupRequest
	{
		public void manipulateClassGroup(Entity entity);
	}

	public static final long DEFAULT_CLASS_ID = 0;
	public static final String DEFAULT_CLASS_NAME = "default_class";

	private Map<Long, ClassGroup> selectedClassGroups;
	private ClassGroupRequest usedClassGroupRequest;

	public ClassGroupPointer()
	{
		selectedClassGroups = new HashMap<>();
		usedClassGroupRequest = null;
	}

	public void selectClassGroup(long... classIDs)
	{
		ClassGroup currentClassGroup;
		for(Long classID: classIDs)
		{
			if((currentClassGroup = Engine.dataManager.getClassGroup(classID)) != null)
			{
				selectedClassGroups.put(classID, currentClassGroup);
			}
		}
	}

	public void removeClassGroup(long... classIDs)
	{
		for(Long classID: classIDs)
		{
			selectedClassGroups.remove(classID);
		}
	}

	public void undoSelection()
	{
		selectedClassGroups.clear();
	}

	public void defineClassGroupRequest(ClassGroupRequest request)
	{
		usedClassGroupRequest = request;
	}

	public void runRequest()
	{
		if(usedClassGroupRequest != null)
		{
			for(ClassGroup classGroup: selectedClassGroups.values())
			{
				for(Entity entity: classGroup.getEntities())
					usedClassGroupRequest.manipulateClassGroup(entity);
			}
		}
	}
}
