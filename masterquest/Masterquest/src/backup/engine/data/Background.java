package engine.data;

import engine.Logic;
import engine.renderEngine.TextureLegacy;


public class Background // This class is for creating optimized background virtualisation out of many images
{
	public static class BackgroundRange
	{
		public Vector2D position; // All sizes are given in units
		public Vector2D dimension;

		public BackgroundRange(Vector2D position, Vector2D dimension)
		{
			this.position = position;
			this.dimension = dimension;
		}

		@Override
		public int hashCode()
		{
			return 1;
		}

		@Override
		public boolean equals(Object o)
		{
			if(o == null)
				return false;
			if(o instanceof BackgroundRange)
			{
				BackgroundRange br = (BackgroundRange) (o);
				return ((Logic.between(br.position.xCoord, position.xCoord - dimension.xCoord / 2, dimension.xCoord) && Logic.between(br.position.yCoord, position.yCoord - dimension.yCoord / 2, dimension.yCoord))
						||
						 Logic.between(position.xCoord, br.position.xCoord - br.dimension.xCoord/2, br.dimension.xCoord) && Logic.between(position.yCoord, br.position.yCoord-br.dimension.yCoord/2, br.dimension.yCoord));
			}
			return false;
		}
	}

	private BackgroundRange backgroundRange;
	private TextureLegacy texture;
	private float blur;

	public Background(BackgroundRange backgroundRange, TextureLegacy texture)
	{
		this.backgroundRange = backgroundRange;
		this.texture = texture;
		blur = 0.0f;
	}

	public void setBlur(float blur) // The blur effect can be configured from 0.0f to 255.99f where 0.0f is no blur effect
	{
		this.blur = blur;
	}

	public BackgroundRange getBackgroundRange()
	{
		return backgroundRange;
	}

	public float getBlur()
	{
		return blur;
	}

	public TextureLegacy getTexture()
	{
		return texture;
	}
}
