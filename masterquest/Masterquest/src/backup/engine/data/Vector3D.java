package engine.data;

public class Vector3D 
{
	public double xCoord;
	public double yCoord;
	public double zCoord;

	
	public static final Vector3D ZERO = new Vector3D(0, 0, 0);
	public static final Vector3D ONE = new Vector3D(1, 1, 1);

	
	public Vector3D()
	{
		this(0, 0, 0);
	}
	
	public Vector3D(Vector3D vector3D)
	{
		this(vector3D.xCoord, vector3D.yCoord, vector3D.zCoord);
	}
	
	public Vector3D(double xCoord, double yCoord, double zCoord)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
	}
	
	
	@Override
	public int hashCode()
	{
		//TODO hashCode algorithm
		/*
		 * Noch nicht: Es werden die ersten 4 Stellen der x- und die ersten 4 Stellen
		 * 			   der y-Koordinate geschrieben. Dazwischen liegt eine 0.
		 * Derweil in Verwendung: Es wird die x-Koordinate durch die y-Koordinate geteilt
		 * und der int-Wert ergibt den hashCode
		 * hashCode = xCoord / yCoord
		 */
		return (int)(xCoord / yCoord);
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object instanceof Vector3D)
		{
			Vector3D vector3D = (Vector3D)(object);
			return ((yCoord == vector3D.yCoord && xCoord == vector3D.xCoord));
		}
		return false;
	}
	
	
	public Vector3D add(Vector3D adder)
	{
		xCoord += adder.xCoord;
		yCoord += adder.yCoord;
		zCoord += adder.zCoord;
		return this;
	}
	public Vector3D div(double adder)
	{
		xCoord /= adder;
		yCoord /= adder;
		zCoord /= adder;
		return this;
	} 
	public Vector3D sub(Vector3D sub)
	{
		xCoord -= sub.xCoord;
		yCoord -= sub.yCoord;
		zCoord -= sub.zCoord;
		return this;
	}
	public Vector3D mul(Vector3D mul)
	{
		xCoord *= mul.xCoord;
		yCoord *= mul.yCoord;
		zCoord *= mul.zCoord;
		return this;
	}
	public Vector3D div(Vector3D div)
	{
		xCoord /= div.xCoord;
		yCoord /= div.yCoord;
		zCoord /= div.zCoord;
		return this;
	}
	
	
	public Vector3D setVector3D(double x, double y, double z)
	{
		this.xCoord = x;
		this.yCoord = y;
		this.zCoord = z;
		return this;
	}
	
	
	public double getYCoord()
	{
		return yCoord;
	}
	
	public double getXCoord()
	{
		return xCoord;
	}

	public double getZCoord()
	{
		return zCoord;
	}
}
