package engine.data;


public class TransformKey extends Transform
{
	//Variablen auf die schnell zugegriffen werden muss
	private static TransformKey t;

	public TransformKey()
	{
		super();
	}
	
	public TransformKey(Vector3D position)
	{
		this(position, null);
	}

	public TransformKey(Vector3D position, Vector3D dimension)
	{
		super(position, dimension, null);
	}
	
	
	public TransformKey addDimension(Vector3D dimension)
	{
		this.dimension = dimension;
		return this;
	}
	
	
	@Override
	public int hashCode()
	{
		return 1;
	}

	@Override
	public boolean equals(Object o)
	{
		if(o instanceof TransformKey)
		{
			t = (TransformKey)(o);
			return this.isIn(t);
		}
		return false;
	}
	
	private boolean isIn(TransformKey transformKey)
	{
		return((transformKey.position.xCoord >= this.position.xCoord && transformKey.position.xCoord <= this.position.xCoord + this.dimension.getXCoord())
				&& (transformKey.position.yCoord >= this.position.yCoord && transformKey.position.yCoord <= this.position.yCoord + this.dimension.getYCoord()));
	}
}
