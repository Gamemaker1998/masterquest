package engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import engine.data.Entity;

public class DataFileReader
{
	// CONSTANTS for loading entities;
	public static final byte WHOLE_ENTITY_READER = 0x0;
	public static final byte CHARAKTER_READER = 0x1;
	public static final byte ENTITY_WIHTOUT_CHARAKTER_READER = 0x2;
	
	public static final String fileExtension = ".entities";
	
	// Variablen auf die schnell zugegriffen werden muss
	private static String line;
	
	
	public ArrayList<Entity> readData(String fileName, int readerType) throws FileNotFoundException, IOException
	{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(fileName.concat(fileExtension))));
		ArrayList<Entity> entities = null;
		if((line = bufferedReader.readLine()) == null)
			return null;
		if(readerType == WHOLE_ENTITY_READER)
		{
			entities = new ArrayList<Entity>(Integer.parseInt(line));
			while((line = bufferedReader.readLine()) != null)
			{
				
			}
		}
		else if(readerType == CHARAKTER_READER)
		{
			entities = new ArrayList<Entity>(Integer.parseInt(line));
			while((line = bufferedReader.readLine()) != null)
			{
				
			}
		}
		else if(readerType == ENTITY_WIHTOUT_CHARAKTER_READER)
		{
			entities = new ArrayList<Entity>(Integer.parseInt(line));
			while((line = bufferedReader.readLine()) != null)
			{
				
			}
		}
		
		return entities;
	}
}
