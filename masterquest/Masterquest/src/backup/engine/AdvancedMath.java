package engine;

public final class AdvancedMath 
{
	public static final double PI = 3.141592653589793238462643383279502884197169399375105820;
			
	public static int roundToNextInt(double d)
	{
		int i = (int)(d);
		return (d > i) ? i + 1 : i;
	}
}
