package engine.input;

import java.awt.MouseInfo;
import java.awt.PointerInfo;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;

import engine.data.Vector2D;


public class InputHandler implements KeyListener, MouseListener, MouseMotionListener
{
	private HashMap<Integer, Boolean> currentKeys;
	private boolean[] keys;
	private byte currentMouse;
	private byte mouse;
	private PointerInfo pointerInfo;

	//Variablen auf die schnell zugegriffen werden muss
	private int keyCode;
	private Vector2D position;

	public InputHandler()
	{
		init();
	}

	public void init()
	{
		currentKeys = new HashMap<Integer, Boolean>();
		keys = new boolean[65536];
		currentMouse = 0;
		mouse = 0;
		pointerInfo = MouseInfo.getPointerInfo();
		position = new Vector2D();
	}

	public void controlls()
	{

	}

	@Override
	public void mouseClicked(MouseEvent m)
	{

	}

	@Override
	public void mousePressed(MouseEvent m)
	{
		currentMouse = 1;
	}

	@Override
	public void mouseReleased(MouseEvent m)
	{
		currentMouse = 0;
	}

	@Override
	public void mouseEntered(MouseEvent m)
	{

	}

	@Override
	public void mouseExited(MouseEvent m)
	{
		
	}

	@Override
	public void keyTyped(KeyEvent k)
	{
		
	}

	@Override
	public void keyPressed(KeyEvent k)
	{
		currentKeys.put(k.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent k)
	{
		currentKeys.put(k.getKeyCode(), false);
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{

	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
	}

	/**
	 * Is not good to use due to the fact that it is deprecated.
	 */
	@Deprecated
	public void refresh()
	{
		for(Map.Entry<Integer, Boolean> entry: currentKeys.entrySet())
		{
			keys[entry.getKey()] = entry.getValue();
		}
		pointerInfo = MouseInfo.getPointerInfo();
		position.xCoord = pointerInfo.getLocation().getX();
		position.yCoord = pointerInfo.getLocation().getY();
		mouse = currentMouse;
	}

	/**
	 * Returns true if the given key (The key code parameter match the key codes of KeyEvent) is pressed
	 * and false if not
	 * @param keyCode
	 * @return
	 */
	public boolean isPressed(int keyCode)
	{
		if(currentKeys.get(keyCode) == null)
			return false;
		return currentKeys.get(keyCode);
	}

	public Vector2D getMousePosition()
	{
		return position.setVector2D(pointerInfo.getLocation().getX(), pointerInfo.getLocation().getY());
	}

	public byte mouseClicked()
	{
		return mouse;
	}
}
