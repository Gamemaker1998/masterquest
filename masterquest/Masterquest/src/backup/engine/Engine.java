package engine;

import java.util.LinkedList;

import engine.data.Entity;
import engine.data.Transform;
import engine.data.Vector3D;
import engine.input.InputHandler;
import engine.physicEngine.PhysicManager;
import engine.renderEngine.RenderManager;
import engine.renderEngine.TextureLegacy;
import engine.renderEngine.shaderEngine.ShaderModel;
import soundEngine.SoundEngine;


public final class Engine extends Process implements Runnable // Das ist der Main-Process
{
	private static Thread mainThread;
	private static boolean run;
	private static RenderThread renderThread;

	public static DataManager dataManager;
	public static RenderManager renderManager;
	public static PhysicManager physicManager;
	public static InputHandler inputHandler;
	public static SoundEngine soundEngine;

	private static boolean dataOperationFlag;

	private static int ticksPerMilliSecond;
	private static int ticksPerMilliSecond2;
	private static int currentTicksPerMilliSecond;
	private static long deltaTime;
	private static long deltaTime2;
	private static int currentTicks;
	private static int currentTicks2;//kommentare sind wichtig
	private static long milliSleeper;

	//guter source-code hat vieeele kommentare
	//#hl3 confirmed
	private static final long FRAME_LIMITER = 1000 / 1000;

	private static TextureLegacy logo;//Texture.loadBitmap("res\\logo\\MasterQuestLogo.png");
	private static long time = 0;
	private static boolean gameMode;

	public static synchronized void init(boolean gM)
	{
		gameMode = gM;

		deltaTime = 0;
		processes = new LinkedList<Process>();

		mainThread = new Thread(new Engine());
		renderThread = new RenderThread();
		milliSleeper = 0;

		dataManager = new DataManager();
		renderManager = new RenderManager();
		physicManager = new PhysicManager();
		inputHandler = new InputHandler();

		renderManager.createScreen("Masterquest Game Engine", 1366, 768);
		renderManager.screen.setVisible(true);
		
		soundEngine = new SoundEngine();
	}

	public static void start()
	{
		mainThread.start();
		run = true;
		renderThread.start();
	}

	public static synchronized void terminate()
	{
		if(run)
		{
			mainThread.interrupt();
		}
	}

	@Override
	public void run()
	{
		if(gameMode)
		{
			Entity eLogo = Entity.createEntity(new Transform(new Vector3D(10000, 10000, 0), new Vector3D(100, 100, 0), new Vector3D(0, 0, 0))).addShaderModel(new ShaderModel(logo));
			renderManager.setRenderingModes(true, true, true, false, false, false, false);
			time = System.currentTimeMillis();

			while(System.currentTimeMillis() - time < 4000) // Wait 4 seconds while displaying the logo

				dataManager.removeEntity(eLogo);
			renderManager.setRenderingModes(true, true, true, true, true, true, true);
			Entity.mainCameraPosition.setVector3D(0.0f, 0.0f, 0.0f);
		}
		while(!mainThread.isInterrupted())
		{
			process();
		}
		
		run = false;
	}

	@Override
	public void process()
	{
		// TODO: Improving the calculation for the ticks per second
		try
		{
			Thread.sleep(milliSleeper);
		}
		catch (Exception e)
		{
		}
		if(deltaTime == 0)
		{
			deltaTime = System.currentTimeMillis();
			currentTicks = 0;
			return;
		}
		else if(deltaTime2 == 0)
		{
			if(System.currentTimeMillis() - deltaTime >= 500)
			{
				deltaTime2 = System.currentTimeMillis();
				currentTicks2 = 0;
			}
			return;
		}
		if(System.currentTimeMillis() - deltaTime >= 1000)
		{
			deltaTime = System.currentTimeMillis();
			ticksPerMilliSecond = currentTicks;
			currentTicksPerMilliSecond = ticksPerMilliSecond;
			System.out.println(ticksPerMilliSecond + " TPS");
			milliSleeper = FRAME_LIMITER;
			currentTicks = 0;
		}
		else if(System.currentTimeMillis() - deltaTime2 >= 1000)
		{
			deltaTime2 = System.currentTimeMillis();
			ticksPerMilliSecond2 = currentTicks2;
			currentTicksPerMilliSecond = ticksPerMilliSecond2;
			milliSleeper = FRAME_LIMITER;
			currentTicks2 = 0;
		}

		if(!dataOperationFlag)
		{
			for(Process process: processes)
			{
				process.process();
			}
			dataManager.manageData();
			physicManager.physics();
		}
		soundEngine.playMusic();
		
		currentTicks++;
		currentTicks2++;
	}

	public static float getDeltaTime()
	{
		return (currentTicksPerMilliSecond == 0) ? (0) : (1.0f / (currentTicksPerMilliSecond));
	}

	public static void setDataOperationFlag(boolean dataOperationFlag)
	{
		Engine.dataOperationFlag = dataOperationFlag;
	}

	static class RenderThread implements Runnable
	{
		Thread thread;
		boolean run;

		public RenderThread()
		{
			thread = new Thread(this);
			thread.setPriority(Thread.MAX_PRIORITY);
			run = false;
		}

		public void start()
		{
			thread.start();
			run = true;
		}

		@Override
		public void run()
		{
			while(!thread.isInterrupted())
			{
				renderManager.render(ticksPerMilliSecond);
			}
			run = false;
		}
	}

}
