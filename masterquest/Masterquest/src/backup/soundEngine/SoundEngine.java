package soundEngine;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;


public class SoundEngine
{
	private AudioClip[] backgroundClips;
	private Random random;
	private AudioClip currentAudioClip;
	private long time;
	private int duration;

	public SoundEngine()
	{
		backgroundClips = new AudioClip[10];
		random = new Random();
		
		try
		{
			for(int i=0;  i<6;  i++)
			{
				backgroundClips[i] = Applet.newAudioClip(new URL("file:music_" + (i+1) + ".wav"));		
				System.out.println(i);
			}
		}
		catch (MalformedURLException e)
		{
			System.err.println("Music wasn't found");
		}
	}
	
	
	public void playMusic()
	{
		if(currentAudioClip == null)
		{
			currentAudioClip = backgroundClips[random.nextInt(6)];
			currentAudioClip.loop();
			time = System.currentTimeMillis();
			duration = (random.nextInt(10)+1)*10000;
		}
		else if((System.currentTimeMillis() - time) > duration)
		{
			currentAudioClip.stop();
			currentAudioClip = backgroundClips[random.nextInt(6)];
			currentAudioClip.loop();
			time = System.currentTimeMillis();
		}
	}
}
