package exception;

public class ToFewArgumentsException extends Exception
{
	public ToFewArgumentsException()
	{
		this("To few arguments given");
	}
	
	public ToFewArgumentsException(String exceptionMessage)
	{
		super(exceptionMessage);
	}
}
