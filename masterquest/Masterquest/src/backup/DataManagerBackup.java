package backup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import engine.data.Background;
import engine.data.Background.BackgroundRange;
import engine.data.Charakter;
import engine.data.Entity;
import engine.data.Monster;
import engine.data.Transform;
import engine.data.Vector2D;
import engine.data.Vector3D;
import engine.renderEngine.TextureLegacy;
import engine.renderEngine.shaderEngine.animation.Animation;


public final class DataManagerBackup
{
	// Interne Datenklassen
	/**
	 * A ClassGroup defines a collection of entities based on the group's id. 
	 * It can also have a name
	 */
	public class ClassGroup
	{
		private long classID;
		private String name;

		private Set<Entity> entities;

		private ClassGroup(long classID, String name)
		{
			this.classID = classID;
			this.name = name;
		}

		@Override
		public int hashCode()
		{
			return (int) (classID);
		}

		public long getClassID()
		{
			return classID;
		}

		public String getName()
		{
			return name;
		}

		public Set<Entity> getEntities()
		{
			return entities;
		}
	}

	/**
	 * Is a collection of entities, characters, monsters and backgrounds to
	 * separate the whole world into smaller areas also called terrains
	 * 
	 * @author yan
	 *
	 */
	private static class Chunk
	{
		private List<Entity> entities;
		private List<Charakter> characters;
		private List<Monster> monsters;
		private List<Background> backgrounds;

		private Vector3D chunkPosition;

		private Chunk(Vector3D chunkPosition)
		{
			entities = new ArrayList<>();
			characters = new ArrayList<>();
			monsters = new ArrayList<>();
			backgrounds = new ArrayList<>();

			this.chunkPosition = chunkPosition;
		}
	}

	private Map<Vector3D, Chunk> chunks;
	private Map<Long, ClassGroup> classGroups;
	private Set<Long> takenIDs;

	private boolean manageData;
	private boolean manipulateData;
	private ArrayList<Chunk> managedChunks;

	public Vector3D protectedMainCameraPosition = new Vector3D(Entity.mainCameraPosition);
	public static final int DEFAULT_UNITS_PER_CHUNK = 1000;

	private Vector3D position;
	private Vector3D sightDistance;

	//Together stored data
	/**
	 * The currentChunkBuffer stores the chunks based on their currently given chunk ID to facilitate the selecting of the chunks
	 */
	private Map<Long, Chunk> currentChunkBuffer;

	
	public DataManagerBackup()
	{
		init();
	}

	private void init()
	{
		chunks = new HashMap<>();
		classGroups = new HashMap<>();
		takenIDs = new HashSet<>();

		managedChunks = new ArrayList<>();
		
		currentChunkBuffer = new HashMap<>();

		//-116.0/-49.0 , 135.0/-31.0
		this.addBackground(new Background(new BackgroundRange(new Vector2D(0, 30), new Vector2D(300, 100)), TextureLegacy.loadBitmap("backgrounds___forest_by_scummy-d4vb4ha.png")));
	}
	
	
	/**
	 * 
	 */
	public long generateEntityID()
	{
		long id;
		Random random = new Random();
		
		while(takenIDs.contains((id = random.nextLong())));
		
		return id;
	}

	/**
	 * This is to add an already defined entity
	 * 
	 * @param entity
	 */
	public void addEntity(Entity entity)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(entity.getTransform().getPosition());

		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			currentChunk = new Chunk(currentChunkPosition);
			chunks.put(currentChunkPosition, currentChunk);
		}
		currentChunk.entities.add(entity);
		manipulateData = false;
	}

	/**
	 * This is to add an already defined character
	 * 
	 * @param character
	 */
	public void addCharakter(Charakter character)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(character.getTransform().getPosition());
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			currentChunk = new Chunk(currentChunkPosition);
			chunks.put(currentChunkPosition, currentChunk);
		}
		currentChunk.characters.add(character);
		manipulateData = false;
	}

	/**
	 * This is to add an already defined monster
	 * 
	 * @param monster
	 */
	public void addMonster(Monster monster)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(monster.getTransform().getPosition());
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			currentChunk = new Chunk(currentChunkPosition);
			chunks.put(currentChunkPosition, currentChunk);
		}
		currentChunk.monsters.add(monster);
		manipulateData = false;
	}

	/**
	 * This is to add an already defined background
	 * 
	 * @param background
	 */
	public void addBackground(Background background)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(background.getBackgroundRange().position);
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			currentChunk = new Chunk(currentChunkPosition);
			chunks.put(currentChunkPosition, currentChunk);
		}
		currentChunk.backgrounds.add(background);
		manipulateData = false;
	}

	/**
	 * This is to remove an already defined entity
	 * 
	 * @param entity
	 */
	public void removeEntity(Entity entity)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(entity.getTransform().getPosition());
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			System.out.println("No entity like this existing");
			return;
		}
		currentChunk.entities.remove(entity);
		manipulateData = false;
	}

	/**
	 * This is to remove an already defined character
	 * 
	 * @param character
	 */
	public void removeCharacter(Charakter character)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(character.getTransform().getPosition());
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			System.out.println("No character like this existing");
			return;
		}
		currentChunk.characters.remove(character);
		manipulateData = false;
	}

	/**
	 * This is to remove an already defined monsTer
	 * 
	 * @param monster
	 */
	public void removeMonster(Monster monster)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(monster.getTransform().getPosition());
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			System.out.println("No monster like this existing");
			return;
		}
		currentChunk.monsters.remove(monster);
		Engine.physicManager.removePhysicsComponent(monster.getPhysicsComponent());
		manipulateData = false;
	}

	/**
	 * This is to remove an already defined background
	 * 
	 * @param background
	 */
	public void removeBackground(Background background)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D currentChunkPosition = calculateChunkPosition(background.getBackgroundRange().position);
		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			System.out.println("No backgrounds like this existing");
			return;
		}
		currentChunk.backgrounds.remove(background);
		manipulateData = false;
	}

	/**
	 * This is to add many entities already defined in an array list
	 * 
	 * @param entities
	 */
	public void addEntities(ArrayList<Entity> entities)
	{
		while(manageData)
			;
		manipulateData = true;
		for(Entity entity: entities)
		{
			addEntity(entity);
		}
		manipulateData = false;
	}

	/**
	 * calculates the chunk's position out of the given coordinates
	 * 
	 * @param position
	 * @return
	 */
	public Vector3D calculateChunkPosition(Vector3D position)
	{
		return new Vector3D(position.xCoord - (position.xCoord % DEFAULT_UNITS_PER_CHUNK), position.yCoord - (position.yCoord % DEFAULT_UNITS_PER_CHUNK), 0);
	}

	public Vector3D calculateChunkPosition(Vector2D position)
	{
		return new Vector3D(position.xCoord - (position.xCoord % DEFAULT_UNITS_PER_CHUNK), position.yCoord - (position.yCoord % DEFAULT_UNITS_PER_CHUNK), 0);
	}

	private void createChunk(Vector3D position, ArrayList<Entity> entities)
	{
		Chunk currentChunk = new Chunk(position);
		currentChunk.entities.addAll(entities);
		chunks.put(position, currentChunk);
	}

	/*
	 * TODO: Reprogramm the algorithm: If someone wants to have the entities and later on the backgrounds, the necessary chunks needed for the
	 * entities should be compared to the new wanted chunks. If there are less chunks needed there will be removed some of the current chunks. 
	 * If more are needed, some chunks should be added to the existing ones and the rest should be removed.
	 */
	/**
	 * By giving it a position and the sight distance from that location it
	 * returns all adjacent entities in an array list
	 * 
	 * @param position
	 * @param sightDistance
	 * @return
	 */
	public ArrayList<Entity> getEntities(Vector3D position, Vector3D sightDistance, boolean mirrow)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D firstChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).sub(sightDistance)));
		Vector3D lastChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).add(sightDistance)));
		Vector3D currentChunkPosition = new Vector3D(firstChunkPosition);

		Chunk currentChunk;
		ArrayList<Entity> entities = new ArrayList<>();

		for(; currentChunkPosition.yCoord <= lastChunkPosition.yCoord; currentChunkPosition.yCoord += DEFAULT_UNITS_PER_CHUNK)
		{
			for(currentChunkPosition.xCoord = firstChunkPosition.xCoord; currentChunkPosition.xCoord <= lastChunkPosition.xCoord; currentChunkPosition.xCoord += DEFAULT_UNITS_PER_CHUNK)
			{
				if((currentChunk = chunks.get(currentChunkPosition)) != null)
				{
					entities.addAll(currentChunk.entities);
					managedChunks.add(currentChunk);
				}
			}
		}
		manipulateData = false;
		return entities;
	}

	public ArrayList<Charakter> getCharacters(Vector3D position, Vector3D sightDistance, boolean mirrow)
	{
		while(manageData)
			;

		manipulateData = true;
		Vector3D firstChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).sub(sightDistance)));
		Vector3D lastChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).add(sightDistance)));
		Vector3D currentChunkPosition = new Vector3D(firstChunkPosition);

		Chunk currentChunk;

		ArrayList<Charakter> characterBuffer = new ArrayList<>();

		for(; currentChunkPosition.yCoord <= lastChunkPosition.yCoord; currentChunkPosition.yCoord += DEFAULT_UNITS_PER_CHUNK)
		{
			for(currentChunkPosition.xCoord = firstChunkPosition.xCoord; currentChunkPosition.xCoord <= lastChunkPosition.xCoord; currentChunkPosition.xCoord += DEFAULT_UNITS_PER_CHUNK)
			{
				if((currentChunk = chunks.get(currentChunkPosition)) != null)
				{
					characterBuffer.addAll(currentChunk.characters);
					characterBuffer.addAll(currentChunk.monsters);
					managedChunks.add(currentChunk);
				}
			}
		}
		manipulateData = false;
		return characterBuffer;
	}

	public ArrayList<Monster> getMonsters(Vector3D position, Vector3D sightDistance, boolean mirrow)
	{
		System.out.println("Neu: " + position.xCoord + " " + position.yCoord + " " + position.zCoord + " " + sightDistance.xCoord + " " + sightDistance.yCoord + " " + sightDistance.zCoord);
		while(manageData)
			;

		Vector3D firstChunkPosition;
		Vector3D lastChunkPosition;

		manipulateData = true;
		if(mirrow)
		{
			firstChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).sub(sightDistance)));
			lastChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).add(sightDistance)));
		}
		else
		{
			firstChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position)));
			lastChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).add(sightDistance)));
		}

		Vector3D currentChunkPosition = new Vector3D(firstChunkPosition);

		Chunk currentChunk;

		ArrayList<Monster> monsterBuffer = new ArrayList<>();

		for(; currentChunkPosition.yCoord <= lastChunkPosition.yCoord; currentChunkPosition.yCoord += DEFAULT_UNITS_PER_CHUNK)
		{
			for(currentChunkPosition.xCoord = firstChunkPosition.xCoord; currentChunkPosition.xCoord <= lastChunkPosition.xCoord; currentChunkPosition.xCoord += DEFAULT_UNITS_PER_CHUNK)
			{
				if((currentChunk = chunks.get(currentChunkPosition)) != null)
				{
					for(Monster monster: currentChunk.monsters)
					{
						if((Math.abs(monster.getTransform().getPosition().xCoord - position.xCoord) <= Math.abs(position.xCoord - sightDistance.xCoord)) && (Math.abs(monster.getTransform().getPosition().yCoord - position.yCoord) <= Math.abs(position.yCoord - sightDistance.yCoord)))
						{
							System.out.println(position.xCoord + " " + position.yCoord + " " + position.zCoord + " " + sightDistance.xCoord + " " + sightDistance.yCoord + " " + sightDistance.zCoord);
							monsterBuffer.add(monster);
						}
					}

					managedChunks.add(currentChunk);
				}
			}
		}
		manipulateData = false;
		return monsterBuffer;
	}

	public ArrayList<Background> getBackground(Vector3D position, Vector3D sightDistance, boolean mirrow)
	{
		while(manageData)
			;
		manipulateData = true;
		Vector3D firstChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).sub(sightDistance)));
		Vector3D lastChunkPosition = new Vector3D(calculateChunkPosition(new Vector3D(position).add(sightDistance)));
		Vector3D currentChunkPosition = new Vector3D(firstChunkPosition);

		ArrayList<Background> backgroundBuffer = new ArrayList<>();
		Chunk currentChunk;

		for(; currentChunkPosition.yCoord <= lastChunkPosition.yCoord; currentChunkPosition.yCoord += DEFAULT_UNITS_PER_CHUNK)
		{
			for(currentChunkPosition.xCoord = firstChunkPosition.xCoord; currentChunkPosition.xCoord <= lastChunkPosition.xCoord; currentChunkPosition.xCoord += DEFAULT_UNITS_PER_CHUNK)
			{
				if((currentChunk = chunks.get(currentChunkPosition)) != null)
				{
					backgroundBuffer.addAll(currentChunk.backgrounds);
					managedChunks.add(currentChunk);
				}
			}
		}
		manipulateData = false;
		return backgroundBuffer;
	}

	/**
	 * Returns the ClassGroup based on the classID
	 * 
	 * @param classID
	 * @return
	 */
	public ClassGroup getClassGroup(long classID)
	{
		return classGroups.get(classID);
	}

	/**
	 * Creates a new ClassGroup if the given classID isn't already in use
	 * 
	 * @param classID
	 * @param name
	 */
	public void createClassGroup(long classID, String name)
	{
		ClassGroup classType = new ClassGroup(classID, name);

	}

	private void moveToDifferentChunk(Chunk currentChunk, Object object)
	{
		Vector3D objectChunkPosition;
		if(object instanceof Charakter)
		{
			Charakter character = (Charakter) (object);
			if((objectChunkPosition = calculateChunkPosition(character.getTransform().getPosition())) != currentChunk.chunkPosition)
			{
				currentChunk.characters.remove(character);
				addCharakter(character);
			}
		}
		else if(object instanceof Entity)
		{
			Entity entity = (Entity) (object);
			if((objectChunkPosition = calculateChunkPosition(entity.getTransform().getPosition())) != currentChunk.chunkPosition)
			{
				currentChunk.characters.remove(entity);
				addEntity(entity);
			}
		}
		else if(object instanceof Background)
		{
			Background background = (Background) (object);
			if((objectChunkPosition = calculateChunkPosition(background.getBackgroundRange().position)) != currentChunk.chunkPosition)
			{
				currentChunk.characters.remove(background);
				addBackground(background);
			}
		}
	}

	public void updateData()
	{
		protectedMainCameraPosition.xCoord = Entity.mainCameraPosition.xCoord;
		protectedMainCameraPosition.yCoord = Entity.mainCameraPosition.yCoord;
		protectedMainCameraPosition.zCoord = Entity.mainCameraPosition.zCoord;
	}

	public synchronized void manageData()
	{
		//TODO: While this isn't working, just out-comment it and set the default chunk size to 1000
		/*for(Chunk chunk: managedChunks)
		{
			for(Charakter character: chunk.characters)
			{
				moveToAnotherChunk(chunk, character);
			}
		}*/
		/*for(Entity entities: chunk.entities)
		{
			moveToAnotherChunk(chunk, entities);
		}*/
		/*for(Background background: chunk.backgrounds)
		{
			moveToAnotherChunk(chunk, background);
		}
		managedChunks.remove(chunk);
		}
		manageData = false;*/
		if(position == null || sightDistance == null)
			return;
		//TODO: When running the next line, the game may shake sometimes because it tries to get the characters while another thread tries to get the characters too
		ArrayList<Charakter> characters = getCharacters(position, sightDistance, true);
		boolean useCharacterDefaultTexture = true;
		Animation currentAnimation;
		for(Charakter character: characters)
		{
			Map<String, Animation> currentAnimationMap = character.getShaderModel().getAnimations();
			for(Map.Entry<String, Animation> entry: currentAnimationMap.entrySet())
			{
				currentAnimation = entry.getValue();
				if(currentAnimation.isAnimated())
				{
					useCharacterDefaultTexture = false;
					currentAnimation.animate();
					character.getShaderModel().setTexture(currentAnimation.getCurrentTexture());
				}
			}
			if(useCharacterDefaultTexture)
				character.getShaderModel().setTexture(character.getShaderModel().getDefaultTexture());
			if(character instanceof Monster)
			{
				Monster monster = (Monster) (character);
				monster.sneak();
			}
		}

	}

	public void setCurrentPosition(Vector3D position)
	{
		this.position = position;
	}

	public void setSightDistance(Vector3D sightDistance)
	{
		this.sightDistance = sightDistance;
	}
}
