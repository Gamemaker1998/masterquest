package tester;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import engine.Engine;
import engine.FIOReader;
import engine.Process;
import engine.data.Charakter;
import engine.data.Entity;
import engine.data.Monster;
import engine.data.Transform;
import engine.data.Vector3D;
import engine.renderEngine.TextureLegacy;
import engine.renderEngine.shaderEngine.ShaderModel;
import engine.renderEngine.shaderEngine.animation.Animation;


public class EngineTester extends Process
{
	static Entity entity;
	static Charakter ninja;
	static long timer = 0;
	static float radiants = 0;
	static double circle = 2 * Math.PI;
	static TextureLegacy attack = TextureLegacy.loadBitmap("laserninja\\laserninja_attack_1.png");
	static TextureLegacy jump = TextureLegacy.loadBitmap("laserninja\\laserninja_jump_8.png");
	static Animation salto;
	static Animation ninjaMovementAnimation;
	static Animation ninjaFightAnimation;
	static boolean spaceBoolean = true;
	static boolean wBoolean = true;
	static int jumps = 0;

	public static void main(String[] args)
	{
		Engine.init(false);

		ninja = Charakter.createCharakter("Master Yee", new Transform(Entity.mainCameraPosition, new Vector3D(1.5, 1.8, 1), new Vector3D(0, 0, 0))).addPhysicsComponent().addShaderModel(new ShaderModel(attack));
		ninja.getPhysicsComponent().setGravity(true);
		ninjaMovementAnimation = new Animation("ninja_movement_animation").setLoop(true);
		ninjaMovementAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_movement_1.png"), 100);
		ninjaMovementAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_movement_2.png"), 100);
		ninjaMovementAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_movement_3.png"), 100);
		ninjaMovementAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_movement_4.png"), 100);
		ninjaMovementAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_movement_5.png"), 100);
		ninjaMovementAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_movement_6.png"), 100);

		salto = new Animation("ninja_jump_salto").setLoop(false);
		salto.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_jump_2.png"), 100);
		salto.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_jump_3.png"), 100);
		salto.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_jump_4.png"), 100);
		salto.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_jump_5.png"), 100);
		salto.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_jump_6.png"), 100);
		salto.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_jump_7.png"), 100);

		ninjaFightAnimation = new Animation("ninja_fight_animation").setLoop(false);
		ninjaFightAnimation.addAnimationPoint(TextureLegacy.loadBitmap("laserninja\\laserninja_attack_2.png"), 100);

		ninja.getShaderModel().addAnimation(ninjaMovementAnimation);
		ninja.getShaderModel().addAnimation(salto);
		ninja.getShaderModel().addAnimation(ninjaFightAnimation);

		/*Monster monster = Monster.createMonster("Quelle", new Transform(new Vector3D(4, 10, 0), new Vector3D(2, 2, 2), new Vector3D(0, 0, 0))).addPhysicsComponent().addShaderModel(new ShaderModel(Texture.loadBitmap("res\\textures\\enemy.png")));
		monster.getPhysicsComponent().setGravity(true);*/

		Process.addProcess(new EngineTester());

		FIOReader fioReader = new FIOReader();
		Engine.dataManager.addEntities(fioReader.read());
		Monster.generateMonsters();
		Engine.start();
	}

	@Override
	public void process()
	{
		if(Engine.inputHandler.isPressed(KeyEvent.VK_A))
		{
			Engine.physicManager.translate(ninja, new Vector3D(-20 * Engine.getDeltaTime(), 0, 0));
			ninja.getTransform().getRotation().setVector3D(0, Math.PI, 0);
			if(ninja.getPhysicsComponent().isGroundCollision())
				ninjaMovementAnimation.startAnimation();
		}
		else if(Engine.inputHandler.isPressed(KeyEvent.VK_D))
		{
			Engine.physicManager.translate(ninja, new Vector3D(20 * Engine.getDeltaTime(), 0, 0));
			ninja.getTransform().getRotation().setVector3D(0, 0, 0);
			if(ninja.getPhysicsComponent().isGroundCollision())
				ninjaMovementAnimation.startAnimation();
		}
		else
		{
			ninjaMovementAnimation.stopAnimation();
		}
		if(Engine.inputHandler.isPressed(KeyEvent.VK_W))
		{
			//TODO: Schlampig
			if(wBoolean)
			{
				if(jumps == 1 || ninja.getPhysicsComponent().isGroundCollision())
				{
					if(jumps == 0)
					{

					}
					else if(jumps == 1)
					{
						ninjaMovementAnimation.stopAnimation();
						salto.startAnimation();
					}
					ninja.getPhysicsComponent().getSpeed().yCoord += 15;
					wBoolean = false;
					jumps++;
					ninja.getPhysicsComponent().setGroundCollision(false);
				}
			}
		}
		else
		{
			if(ninja.getPhysicsComponent().isGroundCollision())
				jumps = 0;
			wBoolean = true;
		}
		if(Engine.inputHandler.isPressed(KeyEvent.VK_SPACE))
		{
			if(spaceBoolean)
			{
				ninjaMovementAnimation.stopAnimation();
				ninjaFightAnimation.startAnimation();
				spaceBoolean = false;
				ArrayList<Monster> monsters = Engine.dataManager.getMonsters(ninja.getTransform().getPosition(), new Vector3D(1, 0.5, 0), true);
				for(Monster monster: monsters)
				{
					Engine.dataManager.removeMonster(monster);
				}
			}
		}
		else
		{
			spaceBoolean = true;
		}
	}
}

// User-Guide Fortschritt
/*
package tester;

import engine.Engine;
import engine.FIOReader;
import engine.Processable;
import engine.data.Entity;
import engine.data.Transform;
import engine.data.Vector3D;
import engine.renderEngine.Texture;
import engine.renderEngine.shaderEngine.ShaderModel;


public class EngineTester implements Processable
{
	static Entity entity;
	public static void main(String[] args)
	{
		Engine.init();
		Engine.start();
		
		entity = Entity.createEntity(new Transform(new Vector3D(10, 10, 10), new Vector3D(4, 4, 4), new Vector3D(0, 0, 0))).addShaderModel(new ShaderModel(Texture.loadBitmap("res\\textures\\Cobbelstone.png")));
	}

	@Override
	public void process()
	{
		
	}
}
*/
//FIOReader fioReader = new FIOReader();
//Engine.dataManager.addEntities(fioReader.read());
