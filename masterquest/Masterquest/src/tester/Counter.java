package tester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


public class Counter
{
	static int files;
	static int lines;
	static BufferedReader br;

	public static void main(String[] args) throws IOException, InterruptedException
	{
		File windows = new File("C:\\Windows");
		System.out.println("There are " + windows.listFiles().length + " start directories/files");

		search(windows, 8);

		System.out.printf("There are %d file(s) and %d line(s) of code", files, lines);
	}

	public static void search(File file, int amountOfThreads) throws InterruptedException
	{
		File[][] folderCollection = new File[amountOfThreads][];
		int counter = 1;
		for(File[] fileArray: folderCollection)
		{
			fileArray = new File[file.listFiles().length / amountOfThreads + (counter / amountOfThreads) * (file.listFiles().length % amountOfThreads)];
			System.arraycopy(file.listFiles(), (file.listFiles().length / amountOfThreads) * (counter - 1), fileArray, 0, file.listFiles().length / amountOfThreads + (counter / amountOfThreads) * (file.listFiles().length % amountOfThreads));
			new SearchThread(fileArray);
			System.out.println(fileArray.length + " " + counter++);
		}
		counter = 1;
		for(Thread thread: SearchThread.threads)
		{
			thread.join();
			System.out.println("Finished thread + " + counter++);
		}
	}

	public static void reader(File startFile) throws IOException, FileNotFoundException
	{
		if(startFile.listFiles() == null)
			return;
		for(File file: startFile.listFiles())
		{
			if(file.isDirectory())
			{
				reader(file);
			}
			files++;
		}
	}

	private static class SearchThread extends Thread
	{
		private File[] fs;

		static ArrayList<Thread> threads = new ArrayList<>();

		public SearchThread(File[] fs)
		{
			this.fs = fs;
			threads.add(this);
			start();
		}

		@Override
		public void run()
		{
			for(File file: fs)
			{
				if(file.isDirectory())
				{
					try
					{
						reader(file);
					}
					catch (IOException e)
					{
						System.err.println("Error");
					}
				}
				files++;
			}
		}
	}
}
