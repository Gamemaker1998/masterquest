package tester;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Generics
{
	private static Map<Long, ArrayList<?>> testMap = new HashMap<>();

	public static void addData(long id, ArrayList<?> data)
	{
		testMap.put(id, data);
	}

	public static ArrayList<?> getData(long id)
	{
		return testMap.get(id);
	}

	public static void main(String[] args)
	{
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("Hello");
		System.out.println(strings.get(0));
		ArrayList<String> copiedStrings = new ArrayList<>(strings);
		copiedStrings.set(0, "olleH");
		System.out.println(strings.get(0) + " " + copiedStrings.get(0));
		//new CanSwim("Schimmer");
		//new CanJump("Jumper");
	}
}

class Pointer<T>
{
	public ArrayList<T> reference;

	public Pointer(long id, ArrayList<T> arrayList)
	{
		this.reference = arrayList;
		Generics.addData(id, arrayList);
	}
}

class CanSwim
{
	private String name;
	private Pointer<CanSwim> dataReference;

	public CanSwim(String name)
	{
		this.name = name;
		ArrayList<CanSwim> swimmers = new ArrayList<>();
		swimmers.add(this);
		dataReference = new Pointer<>(1L, swimmers);
	}

	void swim()
	{
		System.out.println(name + " swims");
	}
}

class CanJump
{
	private String name;
	private Pointer<CanJump> dataReference;

	public CanJump(String name)
	{
		this.name = name;
		ArrayList<CanJump> jumpers = new ArrayList<>();
		jumpers.add(this);
		dataReference = new Pointer<>(2L, jumpers);
	}

	void jump()
	{
		System.out.println(name + "jumps");
	}
}
