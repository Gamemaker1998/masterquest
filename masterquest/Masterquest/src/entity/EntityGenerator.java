package entity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import engine.Engine;
import engine.data.Transform;
import engine.data.Vector3D;
import engine.data.util.vector.Vector3f;
import engine.renderEngine.GraphicsModel;
import engine.renderEngine.RawModel;
import engine.renderEngine.TextureLegacy;
import exception.ToFewArgumentsException;


public class EntityGenerator
{
	private static EntityGenerator instance;

	private static final int DEFAULT_GENERATOR = 0x0;

	public void generateEntityTerrain(Vector3f startPosition, Vector3f endPosition, Vector3f counter, Entity entity, boolean forceMultithreading)
	{
		if(false && forceMultithreading)
			;//new GeneratorThread(startPosition, endPosition, counter, entity, DEFAULT_GENERATOR);
		else
		{
			for(float z = startPosition.z; z <= endPosition.z; z += counter.z)
			{
				for(float y = startPosition.y; y <= endPosition.y; y += counter.y)
				{
					for(float x = startPosition.x; x <= endPosition.x; x += counter.x)
					{
						Entity.createEntity(new Transform(new Vector3f(x, y, z), entity.getTransform().getDimension(), entity.getTransform().getRotation())).setGraphicsModel(new GraphicsModel(RawModel.SQUARE_MODEL, Engine.screen.renderManager.getVAOLoader().createTextureModel("Cobbelstone")));//entity.graphicsModel);
					}
				}
			}
		}
	}
	
	public void generateEntityTerrain(String filename)
	{
		Entity currentEntity;
		File file = new File(filename);
		Vector3f position;
		Vector3f dimension;
		Vector3f rotation;
		BufferedReader bufferedReader = null;
		String DELIMITER = "/";
		int AMOUNT_OF_TOKENS = 7;

		try
		{
			bufferedReader = new BufferedReader(new FileReader(file));
			String line;
			while((line = bufferedReader.readLine()) != null)
			{
				String[] tokens = line.split(DELIMITER);
				if(tokens.length < AMOUNT_OF_TOKENS)
					throw new ToFewArgumentsException("To few arguments in file: " + file.getName());
				position = new Vector3f(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1]), -10.0f);
				dimension = new Vector3f(Float.parseFloat(tokens[2]), Float.parseFloat(tokens[3]), 0.1f);
				rotation = new Vector3f(0.0f, 0.0f, 0.0f);
				//texture = TextureLegacy.loadBitmap("Grass_Mid.png");
					//test2: Entity.mainCameraPosition.setVector3D(position.getXCoord()+10, position.getYCoord()+1, position.getZCoord());
					//testFlo: Entity.mainCameraPosition.setVector3D(position.getXCoord(), position.getYCoord()+10, position.getZCoord());
					//Entity.mainCamera.getTransform().getPosition().setVector3f(position.getXCoord() + 10, position.getYCoord() + 10, position.getZCoord());
				//currentEntity = new Entity(new Transform(position, dimension, rotation)).addShaderModel(new ShaderModel(texture)).addPhysicsComponent();
				Engine.setDataOperationFlag(true);
				//TODO: Doesn't work with the VAOLoader
				System.out.println(position + " " + dimension + " " + rotation);
				Entity.createEntity(new Transform(position, dimension, rotation)).createGraphicsModel(RawModel.SQUARE_MODEL, "Grass_Mid");
				//currentEntity.getPhysicsComponent().setKinetic(Boolean.parseBoolean(tokens[4])).setGravity(Boolean.parseBoolean(tokens[5]));
				//currentEntity.getPhysicsComponent().s = "aaaa";
				//Engine.physicManager.addPhysicsComponent(currentEntity.getPhysicsComponent());
			}
			Engine.setDataOperationFlag(false);
			bufferedReader.close();
		}
		catch (FileNotFoundException e)
		{
			System.err.println("File " + file.getName() + "was not found");
			Engine.setDataOperationFlag(false);
		}
		catch (IOException e)
		{
			System.err.println("IOException at file " + file.getName());
			Engine.setDataOperationFlag(false);
		}
		catch (ToFewArgumentsException e)
		{
			System.err.println(e.getMessage());
			Engine.setDataOperationFlag(false);
		}
		finally
		{
			try
			{
				bufferedReader.close();
			}
			catch (IOException e)
			{
				System.err.println("Error at closing bufferedReader " + e.getMessage());
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/*
	private static class GeneratorThread extends Thread
	{
		private Vector3f startPosition, endPosition, counter;
		private Entity entity;
		private int generatorType;
	
		public GeneratorThread(Vector3f startPosition, Vector3f endPosition, Vector3f counter, Entity entity, int generatorType)
		{
			this.startPosition = startPosition;
			this.endPosition = endPosition;
			this.counter = counter;
			this.entity = entity;
			this.generatorType = generatorType;
	
			setPriority(Thread.MIN_PRIORITY);
			start();
		}
	
		@Override
		public void run()
		{
			switch(generatorType)
			{
			case DEFAULT_GENERATOR:
				instance.defaultGenerator(startPosition, endPosition, counter, entity);
				break;
			}
		}
	}
	
	private void defaultGenerator(Vector3f startPosition, Vector3f endPosition, Vector3f counter, Entity entity)
	{
		for(float z = startPosition.z; z <= endPosition.z; z += counter.z)
		{
			for(float y = startPosition.y; y <= endPosition.y; y += counter.y)
			{
				for(float x = startPosition.x; x <= endPosition.x; x += counter.x)
				{
					Entity.createEntity(new Transform(new Vector3f(x, y, z), entity.getTransform().getDimension(), entity.getTransform().getRotation())).setGraphicsModel(entity.graphicsModel);
				}
			}
		}
	}
	*/
	
	public static EntityGenerator getInstance()
	{
		if(instance == null)
			instance = new EntityGenerator();
		return instance;
	}
}
