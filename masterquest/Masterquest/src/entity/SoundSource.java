package engine.data;

import engine.Engine;


public class SoundSource extends Entity
{
	private Range3D soundRange;
	
	public SoundSource(Transform transform, Range3D soundRange)
	{
		super(Engine.dataManager.generateEntityID(), "-", ClassGroupPointer.SOUND_SOURCE_CLASS_ID, transform);
		this.soundRange = soundRange;
	}
	
	public Range3D getSoundRange()
	{
		return soundRange;
	}
}
