package engine.data;

import java.util.Random;

import engine.Engine;
import engine.renderEngine.TextureLegacy;
import engine.renderEngine.shaderEngine.ShaderModel;


public class Monster extends Charakter
{
	private float side;
	private float speed;
	private long time;
	private int duration;

	protected Monster(String name, Transform transform)
	{
		super(name, transform);
		speed = 1.0f;
		side = 1.0f;
		duration = 1000;
	}

	public static void generateMonsters()
	{
		Random random = new Random();
		Monster monster;
		for(int x = -106; x < 100; x += 1)
		{
			if(random.nextInt(2) == 1)
			{
				monster = createMonster("Qualle", new Transform(new Vector3D(x, 51.0, 0), new Vector3D(1, 1, 1), new Vector3D(0, 0, 0))).addPhysicsComponent().addShaderModel(new ShaderModel(TextureLegacy.loadBitmap("enemy.png")));
				monster.getPhysicsComponent().setGravity(true);
			}
		}
	}

	public static Monster createMonster(String name, Transform transform)
	{
		Monster monster = new Monster(name, transform);
		Engine.dataManager.addEntity(monster);
		return monster;
	}

	@Override
	public Monster addPhysicsComponent()
	{
		super.addPhysicsComponent();
		return this;
	}

	@Override
	public Monster addShaderModel(ShaderModel shaderModel)
	{
		super.addShaderModel(shaderModel);
		return this;
	}

	public void sneak()
	{
		Random random = new Random();
		if(time == 0 || (System.currentTimeMillis() - time) > duration)
		{
			time = System.currentTimeMillis();
			duration = random.nextInt(8) * 250;
			int result = random.nextInt(3);
			if(result == 1)
			{
				side *= -1;
			}
			else if(result == 2)
			{
				result = random.nextInt(2);
				if(result == 1)
					side = -1;
				else
					side = 1;
			}
			else
				duration = 100;
		}
		Engine.physicManager.translate(this, new Vector3D(side * speed * Engine.getDeltaTime(), 0, 0));
	}
}
