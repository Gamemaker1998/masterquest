package engine.data;

import engine.Engine;
import engine.renderEngine.shaderEngine.ShaderModel;
import engine.renderEngine.shaderEngine.animation.Animation;


public class Charakter extends Entity
{
	protected String name;
	protected Animation standbyAnimation;

	protected Charakter(String name, Transform transform)
	{
		super(transform);

		this.name = name;
		standbyAnimation = null;
	}

	public static Charakter createCharakter(String name, Transform transform)
	{
		Charakter currentCharakter = new Charakter(name, transform);
		Engine.dataManager.addEntity(currentCharakter);
		return currentCharakter;
	}
	
	@Override
	public Charakter addShaderModel(ShaderModel shaderModel)
	{
		this.shaderModel = shaderModel;
		return this;
	}
	
	@Override
	public Charakter addPhysicsComponent()
	{
		this.physicsComponent = new PhysicsComponent(transform);
		Engine.physicManager.addPhysicsComponent(physicsComponent);
		return this;
	}
	
	public void addStandbyAnimation(Animation standbyAnimation)
	{
		this.standbyAnimation = standbyAnimation;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
}
