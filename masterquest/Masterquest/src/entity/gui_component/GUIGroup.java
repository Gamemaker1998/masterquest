package entity.gui_component;

import java.util.ArrayList;

import engine.data.Transform;
import engine.data.util.vector.Vector3f;
import entity.Entity;


public class GUIGroup extends Entity
{
	private String groupName;
	private ArrayList<GUIComponent> guiComponents;
	private boolean isSingle;

	public GUIGroup(Transform transform, String groupName)
	{
		super(transform);

		this.groupName = groupName;
		guiComponents = new ArrayList<>();
	}

	public void addGUIComponent(GUIComponent guiComponent)
	{
		isSingle = (guiComponents.size() == 0);
		guiComponents.add(guiComponent);
	}

	public void moveGUIGroup(Vector3f translation)
	{
		isSingle = (guiComponents.size() == 1);
		transform.getPosition().translate(translation.x, translation.y, translation.z);
	}
	
	public String getGroupName()
	{
		return groupName;
	}
	
	public boolean isSingle()
	{
		return isSingle;
	}
}
