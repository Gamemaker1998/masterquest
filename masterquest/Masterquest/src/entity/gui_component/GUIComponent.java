package entity.gui_component;

import engine.data.Transform;
import engine.data.util.vector.Vector3f;
import entity.Entity;


public class GUIComponent extends Entity implements GUIControllable
{
	private boolean isActivated;

	public GUIComponent(Transform transform)
	{
		super(transform);
	}

	public void activate(boolean flag)
	{
		isActivated = flag;
	}

	public boolean isActivated()
	{
		return isActivated;
	}

	@Override
	public void onHover()
	{
		
	}

	@Override
	public void onDrag(Vector3f position)
	{
	
	}

	@Override
	public void onClick()
	{
		
	}
}
