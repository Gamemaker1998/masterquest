package entity.gui_component;

import engine.data.util.vector.Vector3f;


public interface GUIControllable
{
	public void onHover();

	public void onDrag(Vector3f position);

	public void onClick();
}
