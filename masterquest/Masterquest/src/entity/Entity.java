package entity;

import engine.Engine;
import engine.data.ClassGroupPointer;
import engine.data.Transform;
import engine.data.util.vector.Vector3f;
import engine.physicEngine.PhysicsComponent;
import engine.renderEngine.GraphicsModel;
import engine.renderEngine.RawModel;


public class Entity
{
	// Necessary
	protected long ID;
	protected String name;
	protected long classID;
	protected Transform transform;
	// Optional
	protected GraphicsModel graphicsModel;
	protected PhysicsComponent physicsComponent;

	/**
	 * The main camera position is the main view of the user. The whole engine
	 * rendering is based on that position which surely can be changed.
	 */
	public static Camera mainCamera = new Camera(new Transform(new Vector3f(0.0f, 0.0f, 0.0f)));// = new Vector3D(463.0f, 360.0f, 0); // Main-Camera

	protected Entity(Transform transform)
	{
		this(Engine.dataManager.generateEntityID(), "-", ClassGroupPointer.DEFAULT_CLASS_ID, transform);
	}

	protected Entity(long ID, String name, long classID, Transform transform)
	{
		if(!Engine.dataManager.isIDTaken(ID))
		{
			this.ID = ID;
			this.name = name;
			this.classID = classID;
			this.transform = transform;
		}
		else
			System.err.format("The ID(%d) of \"%s\" has already been taken", ID, name);
	}

	public Entity(Entity entity)
	{
		this(entity.ID, entity.name, entity.classID, entity.transform);
	}

	public static Entity createEntity(Transform transform)
	{
		Entity currentEntity = new Entity(transform);
		Engine.dataManager.addEntity(currentEntity);
		return currentEntity;
	}

	public static Entity createEntity(long ID, String name, long classID, Transform transform)
	{
		Entity currentEntity = new Entity(ID, name, classID, transform);
		Engine.dataManager.addEntity(currentEntity);
		return currentEntity;
	}
	
	public static Entity createEntity(Entity entity)
	{
		Entity currentEntity = new Entity(entity);
		Engine.dataManager.addEntity(currentEntity);
		return new Entity(currentEntity);
	}
	
	/**
	 * Every entity has its own run method which is executed by the Engine
	 */
	public void run()
	{

	}

	public Entity createGraphicsModel(float[] positions, float[] textureCoords, int[] indices, String filename)
	{
		graphicsModel = new GraphicsModel(Engine.screen.renderManager.getVAOLoader().createVAOBuffer(positions, textureCoords, indices), Engine.screen.renderManager.getVAOLoader().createTextureModel(filename));
		return this;
	}
	
	public Entity createGraphicsModel(RawModel rawModel, String filename)
	{
		graphicsModel = new GraphicsModel(rawModel, Engine.screen.renderManager.getVAOLoader().createTextureModel(filename));
		return this;
	}

	public Entity setTransfrom(Transform transform)
	{
		this.transform = transform;
		return this;
	}

	public Entity setGraphicsModel(GraphicsModel graphicsModel)
	{
		this.graphicsModel = graphicsModel;
		return this;
	}

	/**
	 * Adds a new physics component to the object meaning that the object gets
	 * its own box collider (if isKinetic is false) and it can handle gravity
	 * (if gravity is true) It automatically adds the physics component to the
	 * physic manager too.
	 * 
	 * @return
	 */
	public Entity addPhysicsComponent()
	{
		this.physicsComponent = new PhysicsComponent(transform);
		Engine.physicManager.addPhysicsComponent(physicsComponent);
		return this;
	}

	public Entity groupEntity(long classID)
	{
		//TODO: Entity groupEntity: The entity should be put into a class group
		this.classID = classID;
		return this;
	}

	public void setTransform(Transform transform)
	{
		this.transform = transform;
	}

	public long getID()
	{
		return ID;
	}

	public String getName()
	{
		return name;
	}

	public long getClassID()
	{
		return classID;
	}

	public Transform getTransform()
	{
		return transform;
	}

	public GraphicsModel getGraphicsModel()
	{
		return graphicsModel;
	}

	public PhysicsComponent getPhysicsComponent()
	{
		return physicsComponent;
	}
}
