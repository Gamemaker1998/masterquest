package entity;

import engine.data.Transform;


public class Camera extends Entity
{
	// Field of view given in degrees
	public final float FOV = 50;
	
	// The pitch describes how low or high the camera is aimed
	private float pitch;
	// How much left or right the camera is aimed
	private float yaw;
	// How much the camera is tilted
	private float roll;

	public Camera(Transform transform)
	{
		super(transform);
	}

	public Camera(Camera camera)
	{
		super(camera.transform);
	}

	public void setPitch(float pitch)
	{
		this.pitch = pitch;
	}

	public void setYaw(float yaw)
	{
		this.yaw = yaw;
	}

	public void setRoll(float roll)
	{
		this.roll = roll;
	}
	
	public void translate(float x, float y, float z)
	{
		transform.getPosition().translate(x, y, z);
	}

	public float getPitch()
	{
		return pitch;
	}

	public float getYaw()
	{
		return yaw;
	}

	public float getRoll()
	{
		return roll;
	}

}
