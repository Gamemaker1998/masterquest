package soundEngine;

/**
 * The MusicBox is responsible for the sounds and musics which are played while the game is running.
 * They are running in the background and do not have a special type of location and range since they are just played when necessary.
 * @author yan
 *
 */
public class MusicBox
{
	private static MusicBox instance;
	
	private MusicBox()
	{
		
	}
	

	public static MusicBox getInstance()
	{
		if(instance == null)
			instance = new MusicBox();
		return instance;
	}
}
