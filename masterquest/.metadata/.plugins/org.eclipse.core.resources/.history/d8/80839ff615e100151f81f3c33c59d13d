package engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import engine.data.util.vector.Vector3f;
import entity.Entity;


public final class DataManager
{
	/**
	 * A ClassGroup defines a collection of entities based on the group's id. It
	 * can also have a name
	 */
	public class ClassGroup<T extends Entity>
	{
		private long classID;
		private String name;

		private ArrayList<T> entities;

		private ClassGroup(long classID, String name)
		{
			this.classID = classID;
			this.name = name;

			entities = new ArrayList<>();
		}

		@Override
		public int hashCode()
		{
			return (int) (classID);
		}

		public long getClassID()
		{
			return classID;
		}

		public String getName()
		{
			return name;
		}

		public ArrayList<T> getEntities()
		{
			return entities;
		}
	}

	/**
	 * Is a collection of entities, characters, monsters and backgrounds to
	 * separate the whole world into smaller areas also called terrains
	 * 
	 * @author yan
	 *
	 */
	private class Chunk
	{
		private Map<Long, HashSet<Entity>> entities;

		private Chunk(Vector3f chunkPosition)
		{
			entities = new HashMap<>();
		}
	}

	private Map<Vector3f, Chunk> chunks;
	private Map<Long, ClassGroup<? extends Entity>> classGroups;
	private Set<Long> takenIDs;

	public static final int DEFAULT_UNITS_PER_CHUNK = 10000;

	public DataManager()
	{
		init();
	}

	private void init()
	{
		chunks = new HashMap<>();
		classGroups = new HashMap<>();
		takenIDs = new HashSet<>();
	}

	/**
	 * 
	 */
	public long generateEntityID()
	{
		long ID;
		Random random = new Random();

		while(isIDTaken(ID = random.nextLong()))
			;

		return ID;
	}

	public boolean isIDTaken(long ID)
	{
		return takenIDs.contains(ID);
	}

	/**
	 * This is to add an already defined entity
	 * 
	 * @param entity
	 */
	public void addEntity(Entity entity)
	{
		//entities.add(entity);
		Vector3f currentChunkPosition = calculateChunkPosition(entity.getTransform().getPosition());

		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			currentChunk = new Chunk(currentChunkPosition);
			chunks.put(currentChunkPosition, currentChunk);
		}

		HashSet<Entity> entities = currentChunk.entities.get(entity.getClassID());
		if(entities == null)
		{
			entities = new HashSet<Entity>();
			currentChunk.entities.put(entity.getClassID(), entities);
		}

		currentChunk.entities.get(entity.getClassID()).add(entity);
	}

	/**
	 * This is to remove an already defined entity
	 * 
	 * @param entity
	 */
	public void removeEntity(Entity entity)
	{
		Vector3f currentChunkPosition = calculateChunkPosition(entity.getTransform().getPosition());

		Chunk currentChunk = chunks.get(currentChunkPosition);
		if(currentChunk == null)
		{
			System.out.println("No entity like this existing");
			return;
		}

		Set<Entity> entities = currentChunk.entities.get(entity.getClassID());
		if(entities == null)
		{
			System.out.println("No entity like this existing");
			return;
		}

		currentChunk.entities.get(entity.getClassID()).remove(entity);
	}

	/**
	 * This is to add many entities already defined in an array list
	 * 
	 * @param entities
	 */
	public void addEntities(Set<Entity> entities)
	{
		throw new RuntimeException("Not supported yet");
	}

	public <T extends Entity> void createSpace(long ID, String name)
	{
		if(classGroups.containsKey(ID))
			System.out.println("ClassGroup: " + ID + " already existing");
		ClassGroup<T> currentClassGroup = new ClassGroup<>(ID, name);
		classGroups.put(ID, currentClassGroup);

	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> ArrayList<T> getSpace(long ID)
	{
		if(classGroups.containsKey(ID))
		{
			return new ArrayList<T>((ArrayList<T>) (classGroups.get(ID).entities));
		}
		return null;
	}
	
	public <T extends Entity> void setSpace(long ID, ArrayList<T> data)
	{
		if(classGroups.containsKey(ID))
		{
			synchronized(classGroups.get(ID).entities)
			{
				
			}
			classGroups.get(ID).entities = (ArrayList<? extends T>)(data);
		}
	}

	public void clearSpace(long ID)
	{
		if(classGroups.containsKey(ID))
			classGroups.remove(ID);
	}

	/**
	 * calculates the chunk's position out of the given coordinates
	 * 
	 * @param position
	 * @return
	 */
	public Vector3f calculateChunkPosition(Vector3f position)
	{
		return new Vector3f(position.x - (position.x % DEFAULT_UNITS_PER_CHUNK), position.y - (position.y % DEFAULT_UNITS_PER_CHUNK), (position.z % DEFAULT_UNITS_PER_CHUNK));
	}

	/**
	 * By giving it a position and the sight distance from that location it
	 * returns all adjacent entities in an array list
	 * 
	 * @param position
	 * @param sightDistance
	 * @return
	 */
	public ArrayList<Entity> getEntities()//Vector3f position, Vector3f sightDistance, boolean mirrow, long[] classGroupID)
	{
		throw new RuntimeException("Not supported yet");
	}
}
