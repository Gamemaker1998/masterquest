package manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import engine.data.ClassGroupPointer;
import engine.data.util.vector.Vector3f;
import entity.Entity;


public final class DataManager
{
	/**
	 * A ClassGroup defines a collection of entities based on the group's id. It
	 * can also have a name
	 */
	public class ClassGroup
	{
		private long classID;
		private String name;

		private ArrayList<Entity> entities;

		private ClassGroup(long classID, String name)
		{
			this.classID = classID;
			this.name = name;

			entities = new ArrayList<>();
		}

		@Override
		public int hashCode()
		{
			return (int) (classID);
		}

		public long getClassID()
		{
			return classID;
		}

		public String getName()
		{
			return name;
		}

		public ArrayList<Entity> getEntities()
		{
			return entities;
		}
	}

	/**
	 * Is a collection of entities, characters, monsters and backgrounds to
	 * separate the whole world into smaller areas also called terrains
	 * 
	 * @author yan
	 *
	 */
	private class Chunk
	{
		private Map<Long, ArrayList<Entity>> entities;

		private Chunk(Vector3f chunkPosition)
		{
			entities = new HashMap<>();
		}
	}

	private Map<Vector3f, Chunk> chunks;
	private Map<Vector3f, Chunk> sharedDataChunks;
	private Set<ClassGroup> classGroups;
	private Set<Long> takenIDs;

	public static final int DEFAULT_UNITS_PER_CHUNK = 1;

	public DataManager()
	{
		init();
	}

	private void init()
	{
		chunks = new HashMap<>();
		classGroups = new HashSet<>();
		takenIDs = new HashSet<>();
	}

	/**
	 * Generates an ID for a new entity
	 */
	public long generateEntityID()
	{
		long ID;
		Random random = new Random();

		while(isIDTaken(ID = random.nextLong()))
			;

		return ID;
	}

	public boolean isIDTaken(long ID)
	{
		return takenIDs.contains(ID);
	}

	/**
	 * This is to add an already defined entity
	 * 
	 * @param entity
	 */
	public void addEntity(Entity entity)
	{
		Vector3f currentChunkPosition = calculateChunkPosition(entity.getTransform().getPosition());

		synchronized(chunks)
		{
			Chunk currentChunk = chunks.get(currentChunkPosition);
			if(currentChunk == null)
			{
				currentChunk = new Chunk(currentChunkPosition);
				chunks.put(currentChunkPosition, currentChunk);
			}

			ArrayList<Entity> entities = currentChunk.entities.get(entity.getClassID());
			if(entities == null)
			{
				entities = new ArrayList<Entity>();
				currentChunk.entities.put(entity.getClassID(), entities);
			}

			entities.add(entity);
			if(entity.getClassID() != ClassGroupPointer.DEFAULT_CLASS_ID)
			{
				if(currentChunk.entities.get(ClassGroupPointer.DEFAULT_CLASS_ID) != null)
				{
					currentChunk.entities.add(ClassGroupPointer.DEFAULT_CLASS_ID, new ArrayList<Entity>());
				}
				currentChunk.entities.get(ClassGroupPointer.DEFAULT_CLASS_ID).add(entity);
			}
		}
	}

	/**
	 * This is to remove an already defined entity
	 * 
	 * @param entity
	 */
	public void removeEntity(Entity entity)
	{
		Vector3f currentChunkPosition = calculateChunkPosition(entity.getTransform().getPosition());

		synchronized(chunks)
		{
			Chunk currentChunk = chunks.get(currentChunkPosition);
			if(currentChunk == null)
			{
				System.out.println("No entity like this existing");
				return;
			}

			ArrayList<Entity> entities = currentChunk.entities.get(entity.getClassID());
			if(entities == null)
			{
				System.out.println("No entity like this existing");
				return;
			}

			currentChunk.entities.get(entity.getClassID()).remove(entity);
		}
	}

	/**
	 * This is to add many entities already defined in an array list
	 * 
	 * @param entities
	 */
	public void addEntities(ArrayList<Entity> entities)
	{
		throw new RuntimeException("Not supported yet");
	}

	/**
	 * calculates the chunk's position out of the given coordinates
	 * 
	 * @param position
	 * @return
	 */
	private Vector3f calculateChunkPosition(Vector3f position)
	{
		return new Vector3f(position.x - (position.x % DEFAULT_UNITS_PER_CHUNK), position.y - (position.y % DEFAULT_UNITS_PER_CHUNK), position.z - (position.z % DEFAULT_UNITS_PER_CHUNK));
	}

	/**
	 * By giving it a position and the sight distance from that location it
	 * returns all adjacent entities in an array list
	 * 
	 * @param position
	 * @param sightDistance
	 * @return
	 */
	public HashSet<Entity> getEntities(Vector3f position, Vector3f sightDistance, long classGroupID)
	{
		Vector3f firstChunkPosition = calculateChunkPosition(Vector3f.sub(position, sightDistance, null));
		Vector3f lastChunkPosition = calculateChunkPosition(Vector3f.add(position, sightDistance, null));
		Vector3f currentChunkPosition = new Vector3f(firstChunkPosition);

		Chunk currentChunk;
		HashSet<Entity> entities = new HashSet<>();

		for(; currentChunkPosition.z <= lastChunkPosition.z; currentChunkPosition.z += DEFAULT_UNITS_PER_CHUNK)
		{
			for(currentChunkPosition.y = firstChunkPosition.y; currentChunkPosition.y <= lastChunkPosition.y; currentChunkPosition.y += DEFAULT_UNITS_PER_CHUNK)
			{
				for(currentChunkPosition.x = firstChunkPosition.x; currentChunkPosition.x <= lastChunkPosition.x; currentChunkPosition.x += DEFAULT_UNITS_PER_CHUNK)
				{
					synchronized(chunks)
					{
						if((currentChunk = chunks.get(currentChunkPosition)) != null)
						{
							if(currentChunk.entities.get(classGroupID) != null)
								entities.addAll(currentChunk.entities.get(classGroupID));
						}
					}
				}
			}
		}
		return entities;
	}
}
