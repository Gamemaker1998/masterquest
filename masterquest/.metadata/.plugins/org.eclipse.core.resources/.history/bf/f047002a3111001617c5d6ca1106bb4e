package engine.renderEngine;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import engine.data.util.vector.Matrix4f;
import engine.data.util.vector.Vector3f;
import engine.renderEngine.shaders.StaticShader;
import entity.Entity;
import toolbox.Maths;


public class Renderer
{
	private int width;
	private int height;
	
	// To have entities appear smaller when the distance to the camera is bigger
	private Matrix4f projectionMatrix;
	
	// Field of view given in degrees
	private static final float FOV = 50;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 1000;

	public Renderer(int width, int height, StaticShader staticShader)
	{
		this.width = width;
		this.height = height;
		createProjectionMatrix();
		staticShader.start();
		staticShader.loadProjectionMatrix(projectionMatrix);
		staticShader.stop();
	}

	/**
	 * It prepares to rendering by clearing the whole screen
	 */
	public void prepare()
	{
		GL11.glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
	}

	public void render(Entity entity, StaticShader staticShader, Vector3f cameraPosition)
	{long time = System.currentTimeMillis();
		GraphicsModel graphicsModel = entity.getGraphicsModel();
		RawModel rawModel = graphicsModel.getRawModel();
		// To do something with a VAO it has to be bound first
		GL30.glBindVertexArray(rawModel.getVaoID());
		// Here the first index of the VAO (the first VBO) is enabled for rendering
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		
		Matrix4f transformationMatrix = Maths.createTransformationMatrix(entity.getTransform().getPosition(), entity.getTransform().getRotation(), entity.getTransform().getDimension());		
		staticShader.loadTransformationMatrix(transformationMatrix);
		
		Matrix4f viewMatrix = Maths.createViewMatrix(new Vector3f(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z));
		staticShader.loadViewMatrix(viewMatrix);
		
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, graphicsModel.getModelTexture().getTextureID());
		
		/* The arrays are rendered by saying that triangles should be renderer,
		 * where it should start rendering and how much vertices are in the current VAO.
		 */

		// In case of rendering just many vertices this method is used
		//GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, model.getVertexCount());

		/*
		 * Because we want to render IntBuffers so Index Buffers (IBs) we need
		 * to draw elements instead of just arrays.
		 * We want to have triangles as the rendering mode, the total amount of vertices,
		 * the type of data we used: In that case it is the Unsigned Int since we used the 
		 * IntBuffer and last but not least the offset where the rendering should start is
		 * zero.
		 */
		
		GL11.glDrawElements(GL11.GL_TRIANGLES, rawModel.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);

		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL30.glBindVertexArray(0);
		System.out.println(System.currentTimeMillis() - time);

	}
	
	private void createProjectionMatrix()
	{
		float aspectRatio = width / height;
		float yScale = (float) ((1.0f / Math.tan(Math.toRadians(FOV / 2.0f))) * aspectRatio);
		float xScale = yScale / aspectRatio;
		float frustum_length = FAR_PLANE - NEAR_PLANE;
		
		projectionMatrix = new Matrix4f();
		projectionMatrix.m00 = xScale;
		projectionMatrix.m11 = yScale;
		projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
		projectionMatrix.m23 = -1f;
		projectionMatrix.m32 = -((2 + NEAR_PLANE * FAR_PLANE) / frustum_length);
		projectionMatrix.m33 = 0;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}
}
