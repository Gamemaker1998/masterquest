package engine.renderEngine;

import java.util.HashSet;
import java.util.Set;

import engine.Engine;
import engine.data.ClassGroupPointer;
import engine.data.util.vector.Vector3f;
import engine.renderEngine.shaders.StaticShader;
import entity.Camera;
import entity.Entity;


public final class RenderManager
{
	private VAOLoader loader;
	private Renderer renderer;
	private StaticShader shader;

	private int framesPerSecond;
	private int frames;
	private long deltaTime;
	
	private Camera currentChosenCamera;
	private List<Entity> currentEntities;

	private static final Vector3f SIGHT_DISTANCE = new Vector3f(20, 20, 20);

	private boolean[] renderingModes; // Enables the needed render options
	private int amountRenderingModes = 7; /* Currently 7 render modes
											* 0) The main renderer
											* 1) Only main renderer
											* 2) Entity renderer
											* 3) Character renderer
											* 4) GUI renderer
											* 5) Background renderer
											* 6) Rendering process renderer
											*/

	public RenderManager(int width, int height)
	{
		loader = new VAOLoader();
		shader = new StaticShader();
		renderer = new Renderer(width, height, shader);
		
		currentChosenCamera = Entity.mainCamera;
		currentEntities = new HashSet<>();

		renderingModes = new boolean[amountRenderingModes];
		setRenderingModes(true, true, true, true, true, true, true);
	}

	public void render()
	{
		if(renderingModes[0])
		{
			if(deltaTime == 0)
			{
				frames = 0;
				framesPerSecond = 10000;
				deltaTime = System.currentTimeMillis();
			}
			if(System.currentTimeMillis() - deltaTime >= 1000)
			{
				deltaTime = System.currentTimeMillis();
				framesPerSecond = frames;
				frames = 0;
				System.out.println(framesPerSecond + " FPS");
			}
			
			// Update the VAOLoader
			loader.updateBuffers();

			// Here the real rendering begins ///////////////////////

			// Activating the shader program
			shader.start();

			// Clear screen
			renderer.prepare();
			
			loadEntities();

			if(renderingModes[1])
			{
				if(renderingModes[5])
					//renderBackground();
				if(renderingModes[2])
					renderEntities();
			}

			// Stopping the shader
			shader.stop();

			frames++;
		}
	}
	
	private void loadEntities()
	{
		currentEntities = Engine.dataManager.getEntities(currentChosenCamera.getTransform().getPosition(), SIGHT_DISTANCE, ClassGroupPointer.DEFAULT_CLASS_ID);
	}

	private void renderEntities()
	{
		Vector3f cameraPosition = new Vector3f(currentChosenCamera.getTransform().getPosition());

		for(Entity entity: currentEntities)
		{	
			if(entity.getGraphicsModel() == null || !entity.getGraphicsModel().isRenderable())
				continue;
			if(entity.getClassID() == ClassGroupPointer.DEFAULT_CLASS_ID)
				renderer.render(entity, shader, cameraPosition);
		}
	}

	private void renderBackground()
	{
		Vector3f cameraPosition = new Vector3f(currentChosenCamera.getTransform().getPosition());

		for(Entity entity: currentEntities)
		{	
			if(entity.getGraphicsModel() == null || !entity.getGraphicsModel().isRenderable())
				continue;
			if(entity.getClassID() == ClassGroupPointer.BACKGROUND_CLASS_ID)
			{
				renderer.render(entity, shader, cameraPosition);
			}
		}
	}

	/**
	 * Currently 7 render modes 0) The main renderer 1) Only main renderer 2)
	 * Entity renderer 3) Character renderer 4) GUI renderer 5) Background
	 * renderer 6) Rendering process renderer
	 */
	public void setRenderingModes(boolean... modes)
	{
		for(int i = 0; i < modes.length && i < amountRenderingModes; i++)
		{
			renderingModes[i] = modes[i];
		}
	}

	/**
	 * Returns the optimal sight distance saying that you just can see the last
	 * objects on the left and the right side
	 */

	public int getFramesPerSecond()
	{
		return framesPerSecond;
	}

	public void end()
	{
		shader.cleanUp();
		loader.cleanUp();
	}
	
	
	public VAOLoader getVAOLoader()
	{
		return loader;
	}
}
